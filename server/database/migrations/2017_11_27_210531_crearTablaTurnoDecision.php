<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaTurnoDecision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turno_decision', function (Blueprint $table){
            $table->increments('idTurnoDecision');
            $table->integer('idDecision')->nullable();
            $table->integer('idTurno')->nullable();
            $table->integer('idJugador')->nullable();
            $table->enum('activo', ['si', 'no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('turno_decision');
    }
}
