<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHistorialBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_balance', function (Blueprint $table) {
            $table->increments('idHistorialBalance');
            $table->integer("idJugador")->nullable();
            $table->integer("idTurno")->nullable();
            $table->integer("idJuego")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historial_balance');
    }
}
