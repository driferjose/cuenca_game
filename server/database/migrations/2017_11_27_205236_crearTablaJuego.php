<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaJuego extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('juego', function (Blueprint $table){
            $table->increments('idJuego');
            $table->integer('balanceInicial')->nullable();
            $table->integer('caudalLimite')->nullable();
            $table->integer('caudalIntermedio')->nullable();
            $table->integer('maxCaudalInt')->nullable();
            $table->integer('minCaudalInt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('juego');
    }
}
