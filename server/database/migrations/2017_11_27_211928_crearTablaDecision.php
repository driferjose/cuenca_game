<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaDecision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decision', function (Blueprint $table) {
            $table->increments('idDecision');
            $table->string('descripcion', 45)->nullable();
            $table->integer('cantidad')->nullable();
            $table->enum('estado', ['activo', 'inactivo']);
            $table->enum('unSoloUso', ['si', 'no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('decision');
    }
}
