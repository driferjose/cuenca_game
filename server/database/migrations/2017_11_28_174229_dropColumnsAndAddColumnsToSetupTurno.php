<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsAndAddColumnsToSetupTurno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('setup_turno', function (Blueprint $table) {
            $table->dropColumn('valor','max','min','descripcion');
            $table->string("nombre",45)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('setup_turno', function (Blueprint $table) {
            //
        });
    }
}
