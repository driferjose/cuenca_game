<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSetupTurno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_turno', function (Blueprint $table) {
            $table->increments('idSetupTurno');
            $table->integer('idTurno')->nullable();
            $table->string('descripcion', 45)->nullable();
            $table->integer('min')->nullable();
            $table->integer('Max')->nullable();
            $table->integer('valor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('setup_turno');
    }
}
