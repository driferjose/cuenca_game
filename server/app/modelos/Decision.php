<?php
namespace server\Modelos;


use Illuminate\Database\Eloquent\Model;

class Decision extends Model
{   

    protected $table = 'decision';
    protected $primaryKey = 'idDecision';
    //public $timestamps = false;
    protected $fillable = [
        'descripcion', 
        'cantidad',
        'estado',
        'unSoloUso'
    ];
    
    protected $hidden = [
    	"updated_at","created_at"
    ];

}