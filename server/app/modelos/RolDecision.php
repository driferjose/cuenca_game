<?php
namespace server\Modelos;


use Illuminate\Database\Eloquent\Model;

class RolDecision extends Model
{   

    protected $table = 'rol_decision';
    protected $primaryKey = array('idRol','idDecision');
    //public $timestamps = false;
    protected $fillable = [
        'idRol', 
        'idDecision',
        'estado'
    ];
    
    protected $hidden = [
    	"updated_at","created_at"
    ];

}