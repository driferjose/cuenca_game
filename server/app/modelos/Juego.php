<?php
namespace server\Modelos;


use Illuminate\Database\Eloquent\Model;

class Juego extends Model
{   

    protected $table = 'juego';
    protected $primaryKey = 'idJuego';
    //public $timestamps = false;
    protected $fillable = [
        'balanceInicial', 
        'caudalLimite',
        'caudalIntermedio',
        'maxCaudalInt',
        'minCaudalInt',
        'maxLluvia',
        'minLluvia',
        'caudalFinal',
        "ingreso",
        'produccionKg',
        'precioPorKg',
        'costoFijo',
        'porcentajePenalidad',
        'porcentajeAumentoIE'
    ];
    
    protected $hidden = [
    	"updated_at","created_at"
    ];

}