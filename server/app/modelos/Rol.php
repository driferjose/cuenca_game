<?php
namespace server\Modelos;


use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{   

    protected $table = 'rol';
    protected $primaryKey = 'idRol';
    //public $timestamps = false;
    protected $fillable = [
        'descripcion', 
        'estado',
    ];
    
    protected $hidden = [
    	"updated_at","created_at"
    ];

}