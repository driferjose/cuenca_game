<?php
namespace server\Modelos;


use Illuminate\Database\Eloquent\Model;

class Jugador extends Model
{   

    protected $table = 'jugador';
    protected $primaryKey = 'idJugador';
    //public $timestamps = false;
    protected $fillable = [
        'idJuego', 
        'idRol',
        'posicion',
        'balance',
        'caudalIntermedio'
    ];
    
    protected $hidden = [
    	"updated_at","created_at"
    ];

}