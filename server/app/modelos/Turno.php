<?php
namespace server\Modelos;


use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{   

    protected $table = 'turno';
    protected $primaryKey = 'idTurno';
    //public $timestamps = false;
    protected $fillable = [
        'nombre',
        'idJuego',
        'lluvia',
        'estado',
        'caudalFinal',
        'caudalIntermedio'
    ];
    
    protected $hidden = [
    	"updated_at","created_at"
    ];

}