<?php
namespace server\Modelos;


use Illuminate\Database\Eloquent\Model;

class TurnoDecision extends Model
{   

    protected $table = 'turno_decision';
    protected $primaryKey = 'idTurnoDecision';
    //public $timestamps = false;
    protected $fillable = [
        'idDecision', 
        'idTurno',
        'idJugador',
        'activo'
    ];
    
    protected $hidden = [
    	"updated_at","created_at"
    ];

}