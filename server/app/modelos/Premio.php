<?php
namespace server\Modelos;


use Illuminate\Database\Eloquent\Model;

class Premio extends Model
{   

    protected $table = 'premio';
    protected $primaryKey = 'idPremio';
    //public $timestamps = false;
    protected $fillable = [
        'idJugador', 
        'idJuego',
        'idTurno',
        'valor'
    ];
    
    protected $hidden = [
    	"updated_at","created_at"
    ];

}