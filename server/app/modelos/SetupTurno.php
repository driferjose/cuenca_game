<?php
namespace server\Modelos;


use Illuminate\Database\Eloquent\Model;

class SetupTurno extends Model
{   

    protected $table = 'setup_turno';
    protected $primaryKey = 'idSetupTurno';
    //public $timestamps = false;
    protected $fillable = [
        'idTurno', 
        'nombre',
        'lluvia'
    ];
    
    protected $hidden = [
    	"updated_at","created_at"
    ];

}