<?php
namespace server\Modelos;


use Illuminate\Database\Eloquent\Model;

class HistorialBalance extends Model
{   

    protected $table = 'historial_balance';
    protected $primaryKey = 'idHistorialBalance';
    //public $timestamps = false;
    protected $fillable = [
        'idJugador', 
        'idTurno',
        'idJuego',
        "balance"
    ];
    
    protected $hidden = [
    	"updated_at","created_at"
    ];

}