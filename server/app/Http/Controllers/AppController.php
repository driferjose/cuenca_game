<?php

namespace server\Http\Controllers;

use server\Http\Controllers\Controller as Controller;
use view;
use Config;
use DB;
use server\modelos\Juego;
use server\modelos\Jugador;
use server\modelos\Turno;
use server\modelos\TurnoDecision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AppController extends Controller
{
    public function home(){
        return view("dashboard");
    }
    public function getData($idJuego=null){
        if($idJuego == null){
            $juego = Juego::where("estado","iniciado")->first();
            $idJuego = $juego->idJuego;
        }
        $arrayTurnos = DB::select("
            SELECT A.idTurno,A.idTurno as pos, A.lluvia,IF(A.idTurno IN(B.idTurno),C.cantidad,0) AS refos 
            FROM turno AS A
            LEFT JOIN turno_decision AS B ON A.idTurno = B.idTurno AND A.idJuego = 1 AND B.idDecision = 3
            LEFT JOIN decision AS C ON C.idDecision = B.idDecision
            ORDER BY A.idTurno");
        $arrayJugadores = Jugador::where("idJuego",$idJuego)
                    ->where("idRol",2)
                    ->select("idJugador")->get();
        $arrayData = array();
        $pos = 1;
        $acciones = DB::table("turno_decision AS A")
            ->join("turno AS B","A.idTurno","=","B.idTurno")
            ->join("juego AS C","B.idJuego","=","C.idJuego")
            ->where([["C.idJuego",1],["A.idDecision",1]])
            ->select(DB::raw("A.idJugador,A.idTurno"))
            ->get();

        $historial_balances = DB::table("historial_balance")
            ->where("idJuego",1)
            ->select(DB::raw("idTurno,idJugador,balance"))
            ->get();
        foreach ($arrayJugadores as $jugador) {
            $dato = json_decode( json_encode($jugador) );
            $dato->pos = $pos;
            $dato->array = array();
            foreach ($arrayTurnos as $turno) {
                $modelo = array("colaboro"=>false,"bono"=> 0 );
                foreach ($acciones as $accion) {
                    if($jugador->idJugador == $accion->idJugador && $turno->idTurno == $accion->idTurno){
                        $modelo["colaboro"] = true;
                    }
                }
                foreach ($historial_balances as $balance) {
                    if($jugador->idJugador == $balance->idJugador && $turno->idTurno == $balance->idTurno){
                        $modelo["bono"] = $this->getFormato($balance->balance);
                    }
                }
                 $dato->array[] = $modelo;
            }
            $arrayData[] = $dato;
            $pos++;
        }
        return array("arrayTurnos"=>$arrayTurnos,"arrayData"=>$arrayData);

    }
    public function getBalance($idJuego=null){

    }

    public function getFormato($numero){
        $numero = "".$numero;
        $ar = explode(".", $numero);
        $numero = $ar[0];
        if( COUNT($ar) ==2 ){
            $decimal = ".".$ar[1];
        }
        else{
            $decimal = "";
        }
        $long = strlen($numero);
        if( $long > 3 ){
            $numero = substr($numero,0, $long-3 ).",".substr($numero, $long-3,3);
        }
        return $numero.$decimal;
    }
}
