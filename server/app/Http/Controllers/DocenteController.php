<?php

namespace server\Http\Controllers;

use server\Http\Controllers\Controller as Controller;
use view;
use Config;
use DB;
use server\modelos\Juego;
use server\modelos\Turno;
use server\modelos\Jugador;
use server\modelos\HistorialBalance;
use server\modelos\Premio;
use server\modelos\TurnoDecision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class DocenteController extends Controller
{      
    public function postConfigurarJuego(){
        $datos = Input::all();
        $datos["juego"]["caudalIntermedio"] = $datos["juego"]["caudalFinal"];
        DB::table("juego")
            ->where("estado","iniciado")
            ->update($datos["juego"]);
        DB::table("decision")
            ->where("descripcion","Aportar")
            ->update(["cantidad" => $datos["aporte"]]);
        DB::table("decision")
            ->where("descripcion","Reforestación")
            ->update(["cantidad"=>$datos["reforestacion"]]);
        DB::table("decision")
            ->where("descripcion","Investigación y Desarrollo")
            ->update(["cantidad"=>$datos["investigacion"]]);
        DB::table("decision")
            ->where("descripcion","Premio")
            ->update(["cantidad"=>$datos["premio"]]);
        foreach ($datos["turnos"] as $turno) {
            DB::table("turno")
                ->where("idTurno",$turno["idTurno"])
                ->update(["lluvia"=>$turno["lluvia"]]);
        }
        return ["status"=>1,"mensaje"=>"Cambios guardados"];
    }

    public function getConfiguracion(){
        $final;
        $final["juego"] = DB::table("juego")
            ->where("estado","iniciado")
            ->select(DB::raw("balanceInicial,caudalLimite,caudalIntermedio,maxCaudalInt,minCaudalInt,maxLluvia,minLluvia,caudalFinal,ingreso,produccionKg,precioPorKg,costoFijo,porcentajePenalidad,porcentajeAumentoIE,varianteCuadal"))
            ->first();
        $final = json_decode(json_encode($final),true);
        $decisiones = DB::table("decision")
            ->whereIn("descripcion",["Aportar","lluvia","Reforestación","Investigación y Desarrollo",'Premio'])
            ->select(DB::raw("descripcion, cantidad"))
            ->get();
        $turnos = DB::table("turno")
            ->select(DB::raw("idTurno,nombre,lluvia"))
            ->get();
        $final["turnos"] = $turnos;
        foreach ($decisiones as $decision) {
            if($decision->descripcion == "Aportar"){
                $final["aporte"] = $decision->cantidad;
            }else if($decision->descripcion == "Reforestación"){
                $final["reforestacion"] = $decision->cantidad;
            }else if($decision->descripcion == "Investigación y Desarrollo"){
                $final["investigacion"] = $decision->cantidad;
            }else if($decision->descripcion == "Premio") {
                $final["premio"] = $decision->cantidad;
            }
        }
        return $final;
    }
    public function postInitJuego(){
    	$datos = Input::all();
        $idPrimerTurno = 1;
        DB::table("turno")
            ->update(['estado' => "no_usado"]);
        $juego = DB::table("juego")
                ->select(DB::raw("caudalFinal,balanceInicial,caudalIntermedio,maxCaudalInt"))
                ->first();
        $turno = DB::table("turno")
                ->where("idTurno",$idPrimerTurno)
                ->select(DB::raw("lluvia,nombre"))
                ->first();
        $caudalFinal = $juego->caudalFinal;
        $hay_reforestacion = DB::table("turno_decision AS A")
            ->join("decision AS B","A.idDecision","=","B.idDecision")
            ->where([
                ["A.idTurno",$idPrimerTurno],
                ["B.descripcion","Reforestación"]
            ])
            ->select(DB::raw("B.cantidad"))
            ->first();
        $update_turno = [
            'estado' => "iniciado"
        ];
        DB::table("turno")
            ->where("idTurno",$idPrimerTurno)
            ->update($update_turno);
        DB::table("jugador")
            ->update(['balance'=>$juego->balanceInicial]);
        DB::table("historial_balance")
            ->delete();
        $jugadoresBalance = DB::table("jugador")
            ->select(DB::raw("idJugador,balance"))
            ->get();
        foreach ($jugadoresBalance as $jugador){
            $historial = [
                "idJugador" => $jugador->idJugador,
                "idTurno" => $idPrimerTurno ,
                "idJuego" => 1,
                "balance" => $juego->balanceInicial
            ];
            //HistorialBalance::create($historial);
        }
        DB::table("premio")
            ->delete();
        DB::table("turno_decision")
            ->delete();
        $caudal_info = DB::table("juego")
                ->where("estado","iniciado")
                ->select(DB::raw("maxCaudalInt,minCaudalInt,caudalLimite,maxLluvia"))
                ->first();
        $respuesta = [
            "status" => 1,
            "data" => [
                "reforestacion" => $hay_reforestacion != null ? 1 : 0,
                "caudal" => ($caudalFinal / $juego->maxCaudalInt) * 100,
                "turno" => [
                    "nombre" => $turno->nombre
                ],
                "medidor" => [
                    "min" => $caudal_info->minCaudalInt,
                    "limite" => $caudal_info->caudalLimite,
                    "max" => $caudal_info->maxCaudalInt
                ],
                "lluvia" => [
                    "valor" => $turno->lluvia,
                    "max" => $caudal_info->maxLluvia
                ]
            ]
            
        ];
        return $respuesta;
    }
    private function obtener_decision($descripcion){
        $decisiones = DB::table("decision")
                        ->where("estado","activo")
                        ->select(DB::raw("idDecision,descripcion,cantidad,unSoloUso"))
                        ->get();
        for ($i=0; $i < count($decisiones); $i++) {
            if($decisiones[$i]->descripcion == $descripcion){
                return $decisiones[$i];
            }
        }
        return null;
    }

    public function getSiguienteTurno($hayReforestacion){
			$finJuego = false;
			$hayCosecha = false;
			$caudal_anterior = 0;
			$turnosCosecha = [8,9,10,11];
			$turnosVerCaudal;
			$config = DB::table("juego")
				->where("estado","iniciado")
				->select(DB::raw("*"))
				->first();
			$turnoActual = DB::table("turno")
				->whereIn("estado",["iniciado","pausado"])
				->select(DB::raw("idTurno,lluvia"))
				->first();
			for ($i=0; $i < count($turnosCosecha); $i++) { 
				if($turnosCosecha[$i] == $turnoActual->idTurno){
					$hayCosecha = true;
				}
			}
			if($hayCosecha){
				$caudal_anterior = DB::table("turno")
					->whereIn("idTurno",[$turnoActual->idTurno - 1,$turnoActual->idTurno - 2])
					->select(DB::raw("caudalFinal"))
					->get();
				$turnosVerCaudal = DB::table('turnos')
					->whereIn('idTurno',[1,2,3,4,5,6,7])
					->select(DB::raw('caudalFinal'))
					->get();
			}
			$siguienteTurno = DB::table("turno")
				->where([
						['idTurno',$turnoActual->idTurno+1],
						["idJuego",$config->idJuego]
				])
				->select(DB::raw("idTurno,nombre,estado,caudalFinal,lluvia"))
				->first();
			//datos de las acciones
			$aportar = DB::table("decision")
				->where([["estado","activo"],["descripcion","Aportar"]])
				->select(DB::raw("idDecision,descripcion,cantidad,unSoloUso"))
				->first();
			$investigar = DB::table("decision")
				->where([["estado","activo"],["descripcion","Investigación y Desarrollo"]])
				->select(DB::raw("idDecision,cantidad"))
				->first();
			$reforestar = DB::table("decision")
				->where("descripcion","Reforestación")
				->select(DB::raw("idDecision,cantidad,unSoloUso"))
				->first();
			$sqlAportadores = "
				SELECT  A.idJugador, A.posicion, A.balance,
				IF(A.idJugador IN (B.idJugador),'si','no') AS aporto
				FROM jugador A 
				LEFT JOIN turno_decision AS B 
				ON A.idJugador = B.idJugador 
				AND  B.idTurno = {$turnoActual->idTurno}
				AND B.idDecision = {$aportar->idDecision}
				WHERE  A.idRol =  2 ";
			$jugadoresAportaron = DB::select($sqlAportadores);
			$premiados = DB::table("premio")
				->where([["idJuego",$config->idJuego],["idTurno",$turnoActual->idTurno]])
				->select(DB::raw("idJugador,valor"))
				->get();
			$jugadorInvestigaEsteTurno = DB::table("turno_decision")
				->where("idDecision",$investigar->idDecision)
				->select(DB::raw("idJugador"))
				->get();
			$jugadoresAuementarPorcentaje = DB::table("juego AS A")
				->join("turno AS B","B.idJuego","=","A.idJuego")
				->join("turno_decision AS C","B.idTurno","=","C.idTurno")
				->where([
						["A.idJuego",$config->idJuego],
						["B.estado","terminado"],
						["C.idDecision",$investigar->idDecision]
				])
				->select(DB::raw("C.idJugador"))
				->get();
						
        $caudal = 0;
        if($turnoActual->idTurno == 1){
					$caudal = $config->caudalFinal;
        }else{
					$turnoAnterior = DB::table("turno")
						->where("idTurno",$turnoActual->idTurno - 1)
						->select(DB::raw("caudalFinal"))
						->first();
					$caudal = $turnoAnterior->caudalFinal;
        }
        foreach ($jugadoresAportaron as $jugador){
					//calculando el ingreso segun la produccion
					$ingreso = 0;
					if($hayCosecha){
						$contador = 0;
						$produccion = $config->produccionKg;
						//aumentar produccion por haber invertido en I&E
						foreach (	$jugadoresAuementarPorcentaje as $jugadorAumentar ) {
							if($jugador->idJugador == $jugadorAumentar->idJugador) {
								$produccion += $produccion * ($config->porcentajeAumentoIE / 100);
							}
						}
						//calcular el ingreso segun el caudal en los turnos en $turnosCosecha
						foreach ($turnosVerCaudal as $turno) {
							if($turno->caudalFinal < $config->caudalLimite) {
								$contador++;
							}
						}
						$porcentajeReduccion = ( $config->porcentajePenalidad * $contador ) / 100;
						$produccion -= $produccion * $porcentajeReduccion;
						$ingreso = ($produccion / count($turnosCosecha)) * $config->precioPorKg;
						if($ingreso < 0) {
							$ingreso = 0;
						}
					}
					//actualizando balance de los jugadores
					$balance = $jugador->balance + $ingreso;
					$balance -= $config->costoFijo;
					if($jugador->aporto == "si"){
						$balance -= $aportar->cantidad;
					}
					
					foreach ($premiados as $premiado) {
							if($jugador->idJugador == $premiado->idJugador){
									$balance += $premiado->valor;
							}
					}
					foreach ($jugadorInvestigaEsteTurno as $investigador) {
							if($jugador->idJugador == $investigador->idJugador) {
									$balance -= $investigar->cantidad;
							}
					}
					if($balance < 0){
							$balance = 0;
					}
					DB::table("jugador")
							->where("idJugador",$jugador->idJugador)
							->update(["balance"=>$balance]);
					$historial = [
							"idJugador" => $jugador->idJugador,
							"idTurno" => $turnoActual->idTurno ,
							"idJuego" => $config->idJuego,
							"balance" => $balance
					];
					HistorialBalance::create($historial);
        }
        //actualizando el caudal del turno actual
        $cIntermedio = 0;

        foreach ($jugadoresAportaron as $jugador){
            if($jugador->aporto == "si"){
                $cIntermedio += $config->varianteCuadal;
            }else{
                $cIntermedio += $caudal > $config->caudalLimite ? 0 : -$config->varianteCuadal;
            }
        }
        $caudal += $turnoActual->lluvia + $cIntermedio;
        $HayReforestarEnEsteTurno = DB::table("turno_decision")
            ->where([["idTurno",$turnoActual->idTurno],["idDecision",$reforestar->idDecision]])
            ->select(DB::raw("COUNT(*) AS total"))
            ->first();
        if($HayReforestarEnEsteTurno->total == 1){
            $caudal += $reforestar->cantidad;
        }
        //validar que el caudal no pase los limites
        if($caudal > $config->maxCaudalInt){
            $caudal = $config->maxCaudalInt;
        }else if($caudal < $config->minCaudalInt){
            $caudal = $config->minCaudalInt;
        }
        DB::table("turno")
            ->whereIn("estado",["iniciado","pausado"])
            ->update(["caudalFinal"=>$caudal,"estado"=>"terminado"]);
        //iniciando el segundo turno
        if($siguienteTurno != null){
            if((int)$hayReforestacion == 1){
                $idRolDocente = 1;
                $docente = DB::table("jugador")
                    ->where([["idJuego",$config->idJuego],["idRol",$idRolDocente]])
                    ->select(DB::raw("idJugador"))
                    ->first();
                $turnoDecision = [
                    "idDecision"=>$reforestar->idDecision,
                    "idTurno"=> $siguienteTurno->idTurno ,
                    "idJugador"=>$docente->idJugador
                ];
                TurnoDecision::create($turnoDecision);
            }
            DB::table('turno')
                ->where('idTurno',$siguienteTurno->idTurno)
                ->update(['estado' => "iniciado"]);
        }else{
            $finJuego = true;
        }
        $respuesta = [
            "status"=> 1,
            "finJuego" => $finJuego,
            "data" => [
                "lluvia" => $siguienteTurno != null ? $siguienteTurno->lluvia : 0,
                "reforestacion" => (int)$hayReforestacion,
                "caudal" => ($caudal / $config->maxCaudalInt) * 100,
                "turno" => $siguienteTurno != null ? $siguienteTurno->nombre : 0
            ]
        ];
        return $respuesta;
    }

    public function getPausarTurno(){
        $turno = DB::table("turno")
            ->where("estado","iniciado")
            ->select(DB::raw("COUNT(*) AS total"))
            ->first();
        if($turno->total > 0){
            DB::table('turno')
                ->where('estado',"iniciado")
                ->update(['estado' => "pausado"]);
        }else{
            DB::table('turno')
                ->where('estado',"pausado")
                ->update(['estado' => "iniciado"]);
        }
        return ["status"=>1,"mensaje"=>"Exitoso"];
    }

    public function getJuego($idJuego=null){
        if($idJuego){
            return view("docente",["idJuego"=>$idJuego]);
        }else{
            $parametros = DB::table("juego")
                ->where("estado","iniciado")
                ->select(DB::raw("maxCaudalInt,minCaudalInt"))
                ->first();
            $variables = [
                "medidor"=>[
                    "max"=>$parametros->maxCaudalInt,
                    "min" => $parametros->minCaudalInt
                ]
            ];
            return view("docente",$variables);
        }
    }

    public function getPremio(){
        $juegoActual = DB::table("turno AS A")
            ->join("juego AS B","A.idJuego","=","B.idJuego")
            ->where("B.estado","=","iniciado")
            ->whereIn("A.estado",["iniciado","pausado"])
            ->select(DB::raw("A.idTurno,B.idJuego,A.caudalFinal"))
            ->first();
        $siguienteTurno = DB::table("turno")
            ->where([
                ['idTurno',$juegoActual->idTurno+1],
                ["idJuego",$juegoActual->idJuego]
            ])
            ->select(DB::raw("idTurno,nombre,estado,caudalFinal,lluvia"))
            ->first();
        if($siguienteTurno == null){
            return ["status"=> 0,"mensaje"=>"Este es el último turno"];
        }
        $premio = DB::table("decision")
                ->where([["estado","activo"],["descripcion","Premio"]])
                ->select(DB::raw("idDecision,cantidad"))
                ->first();
        $aportar = DB::table("decision")
                ->where([["estado","activo"],["descripcion","Aportar"]])
                ->select(DB::raw("idDecision,cantidad"))
                ->first();
        $idDecision = $aportar->idDecision;
        $sql_aportadores = "
            SELECT A.idJugador, COUNT(B.idTurnoDecision) as cant
            FROM jugador A 
            LEFT JOIN turno_decision B ON A.idJugador = B.idJugador AND B.idDecision = $idDecision
            JOIN juego C ON A.idJuego = C.idJuego
            WHERE C.estado = 'iniciado'
            GROUP BY A.idJugador ORDER BY cant DESC";
        $aportadores = DB::select($sql_aportadores);
        $contador  = 0;
        $ganadores = [];
        $numMayor = 0;
        foreach ($aportadores as $aportador) {
            if($contador == 0){
                $numMayor = $aportador->cant;
                $ganadores[] = $aportador;
            }else{
                if($aportador->cant == $numMayor){
                    $ganadores[] = $aportador;
                }
            }
            $contador++;
        }
        $premio_repartido = $premio->cantidad / count($ganadores);
        for($i=0; $i < count($ganadores); $i++){
            $guardar = [
                "idJugador"=>$ganadores[$i]->idJugador,
                "idJuego" => $juegoActual->idJuego,
                "idTurno"=>$juegoActual->idTurno+1,
                "valor" => $premio_repartido
            ];
            Premio::create($guardar);
        }
        return ["status"=>1,"mensaje"=>"Exitoso"];
    }

    public function getActualizarJugadores(){
        $juegoTurnoActual = DB::table("turno AS A")
            ->join("juego AS B","A.idJuego","=","B.idJuego")
            ->where("B.estado","=","iniciado")
            ->whereIn("A.estado",["iniciado","pausado"])
            ->select(DB::raw("A.idTurno,B.idJuego,A.lluvia,A.nombre,A.caudalFinal"))
            ->first();
        $idTurno = $juegoTurnoActual->idTurno;
        $idJuego = $juegoTurnoActual->idJuego;
        $sql_aportadores = "
            SELECT  A.idJugador, A.posicion, A.balance, A.caudalIntermedio,
            IF(B.idDecision = 1 ,'si','no') AS aporto, IF(B.idDecision = 6 ,'si','no') AS noAporto
            FROM jugador A 
            LEFT JOIN turno_decision AS B 
            ON A.idJugador = B.idJugador 
            AND  B.idTurno = $idTurno
            WHERE A.idRol = 2 ";
        $jugadores = DB::select($sql_aportadores);
        $inviertieron = DB::table("turno_decision AS A")
            ->join("turno AS B","A.idTurno","=","B.idTurno")
            ->where([["A.idDecision",2],["B.idJuego",$idJuego]])
            ->select(DB::raw("A.idJugador"))
            ->get();
        $jugadores = json_decode(json_encode($jugadores),true);
        $premiados = DB::table("premio")
            ->where("idTurno",$idTurno)
            ->select(DB::raw("idJugador"))
            ->get();
        for ($i=0; $i < count($jugadores); $i++){
            $jugadores[$i]["invirtio"] = "no";
            $jugadores[$i]["premio"] = "no";
            $jugadores[$i]["balance"] = $this->getFormato($jugadores[$i]["balance"]);
            for ($e=0; $e < count($inviertieron); $e++) {
                if($jugadores[$i]["idJugador"] == $inviertieron[$e]->idJugador){
                    $jugadores[$i]["invirtio"] = "si"; 
                }
            }
            for ($a=0; $a < count($premiados); $a++) {
                $idJugadorPremio = $premiados[$a]->idJugador;
                $idJugador = $jugadores[$i]["idJugador"];
                 if( $idJugador == $idJugadorPremio){
                    $jugadores[$i]["premio"] = "si"; 
                }
            }
        }
        $respuesta = ["status" => 1,
                        "data" => [
                            "jugadores"=> $jugadores
                        ]
        ];
        return $respuesta;
    }

    public function getJugadores(){
         $juegoTurnoActual = DB::table("turno AS A")
            ->join("juego AS B","A.idJuego","=","B.idJuego")
            ->where("B.estado","=","iniciado")
            ->whereIn("A.estado",["iniciado","pausado"])
            ->select(DB::raw("A.idTurno,B.idJuego"))
            ->first();
        $jugadores = DB::table("jugador")
                    ->where([["idJuego",$juegoTurnoActual->idJuego],["idRol","=",2]])
                    ->select(DB::raw("idJugador,posicion"))
                    ->get();
        return ["status"=>1,"jugadores"=>$jugadores];
    }
    public function getFormato($numero){
        $numero = "".$numero;
        $ar = explode(".", $numero);
        $numero = $ar[0];
        if( COUNT($ar) ==2 ){
            $decimal = ".".$ar[1];
        }
        else{
            $decimal = "";
        }
        $long = strlen($numero);
        if( $long > 3 ){
            $numero = substr($numero,0, $long-3 ).",".substr($numero, $long-3,3);
        }
        return $numero.$decimal;
        // $numero = "".$numero;
        // $long = strlen($numero);
        // if( $long > 3 ){
        //     return substr($numero,0, $long-3 ).",".substr($numero, $long-3,3);
        // }
        // return $numero;
        // $miles = floor($numero/1000);
        
        // if($miles>0){
        //     $resto = $numero%1000;
        //     return $miles.",".$resto;
        // }
        // return $numero;
    }
}
