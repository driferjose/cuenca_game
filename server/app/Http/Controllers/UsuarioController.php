<?php

namespace server\Http\Controllers;

use server\Http\Controllers\Controller as Controller;
use view;
use Config;
use DB;
use server\modelos\Juego;
use server\modelos\Turno;
use server\modelos\TurnoDecision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UsuarioController extends Controller
{
    public function iniciar($id){
        if($id > 3 ){
            return view("welcome");
        }
         $decisiones = DB::table("decision")
            ->whereIn("descripcion",["Aportar","Investigación y Desarrollo"])
            ->select(DB::raw("descripcion,cantidad"))
            ->get();
        $resultado = [];
        foreach ($decisiones as $decision) {
            if($decision->descripcion == "Aportar"){
                $resultado["aportar"] = $this->getFormato($decision->cantidad); 
            }else if($decision->descripcion == "Investigación y Desarrollo"){
                $resultado["investigar"] = $this->getFormato($decision->cantidad);
            }
        }
        $caudal_info = DB::table("juego")
                ->where("estado","iniciado")
                ->select(DB::raw("maxCaudalInt,minCaudalInt"))
                ->first();
        $juego = [
            "jugador"=>$id, 
            "decisiones" => $resultado,
            "medidor" => [
                "min" => $caudal_info->minCaudalInt,
                "max" => $caudal_info->maxCaudalInt
            ]
        ];
        return view('usuario',$juego);
    }

    private function obtener_decision($descripcion){
        $decisiones = DB::table("decision")
                        ->where("estado","activo")
                        ->select(DB::raw("idDecision,descripcion,cantidad,unSoloUso"))
                        ->get();
        for ($i=0; $i < count($decisiones); $i++) {
            if($decisiones[$i]->descripcion == $descripcion){
                return $decisiones[$i];
            }
        }
        return null;
    }
    public function getFormato($numero){
        $numero = "".$numero;
        $ar = explode(".", $numero);
        $numero = $ar[0];
        if( COUNT($ar) ==2 ){
            $decimal = ".".$ar[1];
        }
        else{
            $decimal = "";
        }
        $long = strlen($numero);
        if( $long > 3 ){
            $numero = substr($numero,0, $long-3 ).",".substr($numero, $long-3,3);
        }
        return $numero.$decimal;
    }
    public function getListarJugadores(){
        $jugadores = DB::table("jugador")
            ->where("idRol",2)
            ->select(DB::raw("idJugador,posicion"))
            ->get();
        $decisiones = DB::table("decision")
            ->whereIn("descripcion",["Aportar","Investigación y Desarrollo"])
            ->select(DB::raw("descripcion,cantidad"))
            ->get();
        $resultado = [];
        foreach ($decisiones as $decision) {
            if($decision->descripcion == "Aportar"){
                $resultado["aportar"] = $this->getFormato($decision->cantidad); 
            }else if($decision->descripcion == "Investigación y Desarrollo"){
                $resultado["investigar"] = $this->getFormato($decision->cantidad);
            }
        }
        $caudal_info = DB::table("juego")
                ->where("estado","iniciado")
                ->select(DB::raw("maxCaudalInt,minCaudalInt,caudalLimite,maxLluvia"))
                ->first();
        return [
            "status"=>1,
            "jugadores"=>$jugadores,
            "decisiones" => $resultado,
            "medidor" => [
                "min" => $caudal_info->minCaudalInt,
                "limite" => $caudal_info->caudalLimite,
                "max" => $caudal_info->maxCaudalInt
            ],
            "lluvia" => [
                "max" => $caudal_info->maxLluvia
            ]
        ];
    }
    public function getJuegoEstado(){
        $turnoActual = DB::table("turno")
            ->whereIn("estado",["iniciado","pausado"])
            ->select(DB::raw("idTurno,nombre"))
            ->first();
        return [
            "status" => $turnoActual != null ? 1 : 2 ,
            "turno" => $turnoActual != null ? $turnoActual->nombre: "Fin"
        ];
    }
    public function getInfo($idJugador){
        $config = DB::table("juego")
            ->where("estado","iniciado")
            ->select(DB::raw("*"))
            ->first();
        $turnoActual = DB::table("turno")
            ->whereIn("estado",["iniciado","pausado"])
            ->select(DB::raw("idTurno,lluvia,nombre"))
            ->first();
        $turnoAnterior = DB::table("turno")
            ->where("idTurno",$turnoActual->idTurno-1)
            ->select(DB::raw("caudalFinal"))
            ->first();
        $caudal;
        if($turnoActual->idTurno == 1){
            $caudal = $config->caudalFinal;
        }else{
            $caudal = $turnoAnterior->caudalFinal;
        }
        $jugador = DB::table("jugador")
            ->where([
                ["idJuego",$config->idJuego],
                ["idRol","=",2],
                ["idJugador",$idJugador]
            ])
            ->select(DB::raw("balance,posicion"))
            ->first();
        $reforestar = DB::table("decision AS A")
            ->join("turno_decision AS B","A.idDecision","=","B.idDecision")
            ->where([["A.descripcion","Reforestación"],["B.idTurno",$turnoActual->idTurno]])
            ->select(DB::raw("A.idDecision,A.cantidad"))
            ->first();
        $premio = DB::table("premio")
            ->where([["idJugador",$idJugador],["idTurno",$turnoActual->idTurno]])
            ->select(DB::raw("valor"))
            ->first();
        $caudalFinal = ($caudal / $config->maxCaudalInt) * 100;
        $investigar = DB::table("decision")
            ->where([["estado","activo"],["descripcion","Investigación y Desarrollo"]])
            ->select(DB::raw("idDecision,cantidad"))
            ->first();
        $tieneAumento = DB::table("juego AS A")
            ->join("turno AS B","B.idJuego","=","A.idJuego")
            ->join("turno_decision AS C","B.idTurno","=","C.idTurno")
            ->where([
                ["A.idJuego",$config->idJuego],
                ["B.estado","terminado"],
                ["C.idJugador",$idJugador],
                ["C.idDecision",$investigar->idDecision]
            ])
            ->select(DB::raw("C.idJugador"))
            ->first();
        $respuesta = [
            "status" => 1,
            "data" => [
                "lluvia"=> $turnoActual->lluvia,
                "lluvia_max" => $config->maxLluvia,
                "balance" => $this->getFormato($jugador->balance),
                "posicion" => $jugador->posicion,
                "reforestar"=> $reforestar != null ? $reforestar->cantidad : 0,
                "caudal" => $caudalFinal,
                "aumento" => $tieneAumento != null ? 1: 0,
                "premio" => $premio != null ? $premio->valor : 0,
                "turno" => [
                    "nombre"=>$turnoActual->nombre,
                    "id"=>$turnoActual->idTurno
                ]
            ]
        ];
        return $respuesta;
    }

    public function getInvestigar($idJugador){
        $turnosBloqueados = [8,9,10,11,12];
        $juegoTurno = DB::table("turno AS A")
            ->join("juego AS B","A.idJuego","=","B.idJuego")
            ->where("B.estado","=","iniciado")
            ->whereIn("A.estado",["iniciado","pausado"])
            ->select(DB::raw("A.idTurno,B.idJuego,A.estado"))
            ->first();
        for ($i=0; $i < count($turnosBloqueados); $i++) {
            if($juegoTurno->idTurno == $turnosBloqueados[$i]){
                return ["status"=> 0,"mensaje"=>"No puedes investigar en este turno"];
            }
        }
        $investigar = DB::table("decision")
            ->where([["estado","activo"],["descripcion","Investigación y Desarrollo"]])
            ->select(DB::raw("idDecision,descripcion,cantidad,unSoloUso"))
            ->first();
        $turnoDecision = DB::table("turno_decision")
            ->where([
                    ["idJugador",$idJugador],
                    ["idTurno",$juegoTurno->idTurno],
                    ["idDecision",$investigar->idDecision]
            ])
            ->select(DB::raw("COUNT(*) AS total"))
            ->first();
        if($turnoDecision->total > 0 ){
            return ["status"=> 0,"mensaje"=>"Ya no puedes investigar"];
        }
        if($juegoTurno->estado == "pausado"){
            return ["status"=>1,"mensaje"=>"Juego en pausa"];
        }
        $permitirInvestigar = true;
        if($investigar->unSoloUso == "si"){
            $validar = DB::table("juego AS A")
                ->join("turno AS B","B.idJuego","=","A.idJuego")
                ->join("turno_decision AS C","B.idTurno","=","C.idTurno")
                ->where([
                    ["A.idJuego",$juegoTurno->idJuego],
                    ["C.idJugador",$idJugador],
                    ["idDecision",$investigar->idDecision]
                ])
                ->select(DB::raw("C.idJugador"))
                ->first();
            if($validar != null){
                $permitirInvestigar = false; 
            }
        }
        if($permitirInvestigar){
            $guardarTurnoDecision = [
                "idDecision"=>$investigar->idDecision,
                "idTurno" => $juegoTurno->idTurno,
                "idJugador" => $idJugador
            ];
            TurnoDecision::create($guardarTurnoDecision);
            $jugador = DB::table("jugador")
                ->where("idJugador",$idJugador)
                ->select(DB::raw("balance"))
                ->first();
            return ["status"=>1,"mensaje"=>"Exitoso"];
        }else{
            return ["status"=>0,"mensaje"=>"Ya no puedes investigar"];
        }
    }

    public function getAportar($idJugador,$valor_aporte){
        $balance = [];
        $juegoTurno = DB::table("turno AS A")
                ->join("juego AS B","A.idJuego","=","B.idJuego")
                ->where("B.estado","=","iniciado")
                ->whereIn("A.estado",["iniciado","pausado"])
                ->select(DB::raw("A.idTurno,A.estado,B.idJuego,B.caudalLimite,B.caudalIntermedio,A.caudalFinal"))
                ->first();
        $aportar = DB::table("decision")
                ->where([["estado","activo"],["descripcion","Aportar"]])
                ->select(DB::raw("idDecision,descripcion,cantidad,unSoloUso"))
                ->first();
        $turnoDecision = DB::table("turno_decision")
            ->where([
                    ["idJugador",$idJugador],
                    ["idTurno",$juegoTurno->idTurno],
            ])
            ->whereIn("idDecision",[$aportar->idDecision,6])
            ->select(DB::raw("COUNT(*) AS total"))
            ->first();
        if($turnoDecision->total > 0 ){
            return ["estado"=> 0,"mensaje"=>"Ya has aportado"];
        }
        if($juegoTurno->estado == "pausado"){
            return ["estado"=> 0,"mensaje"=>"Juego en pausa"];
        }
        $jugador = DB::table("jugador")
                ->where("idJugador",$idJugador)
                ->select(DB::raw("balance,caudalIntermedio"))
                ->first();
        $cIntermedio = $jugador->caudalIntermedio;
        $jBalance = $jugador->balance;
        $cAporte = $aportar->cantidad;
        $climit = $juegoTurno->caudalLimite;
        $newCaudalInter ;
        if($valor_aporte == 1){
            $guardarTurnoDecision = [
                "idDecision"=>$aportar->idDecision,
                "idTurno" => $juegoTurno->idTurno,
                "idJugador" => $idJugador
            ];
            $guardarDecision = TurnoDecision::create($guardarTurnoDecision);
        }else{
            $guardarTurnoDecision = [
                "idDecision"=>6,
                "idTurno" => $juegoTurno->idTurno,
                "idJugador" => $idJugador
            ];
            $guardarDecision = TurnoDecision::create($guardarTurnoDecision);
        }
        return ["status"=>1,"mensaje"=>"Exitoso"];
    }
    

}
