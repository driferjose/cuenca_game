<?php
header("Access-Control-Allow-Origin:*");
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
header('Access-Control-Allow-Headers: x-xsrf-token, x_csrftoken');

// Route::get('/', function (){
//     return view('juego');
// });
Route::controller('docente',"DocenteController");
Route::controller('app',"AppController");
Route::get('/jugador/{idJugador}',"UsuarioController@iniciar");
Route::controller('usuario',"UsuarioController");
Route::get('/',"AppController@home");
//Route::controller('juego',"UsuarioController");
//Route::get('/admin',"JuegoController@getJuegoAdmin");
