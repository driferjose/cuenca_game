appAngular.controller('jugadorController',['$scope',"$http","$timeout","servicio", 
    function($scope,$http,$timeout,servicio){
    
    let host = "http://localhost/simulador_cuenca/server/public";
    $scope.ctrl_ini = {};
    $scope.jugadores;
    $scope.turno;
    $scope.valReforestar;
    $scope.idTurno = 0;
    $scope.balance = 0;
    $scope.premio = 0;
    $scope.jugadorPosicion = 0;
    $scope.caudal;
    $scope.medidor = [];
    $scope.fin_juego = false;
    $scope.aportarBtn = false;
    $scope.noAportar = false;
    $scope.riego = false;
    $scope.tieneAumento = 0;
    $scope.lluvia = 0;
    $scope.lluviaMax= 0;
    $scope.limite = 0;
    $scope.textoLimite = "";
    
    $scope.animar_lluvia = function(){
        let gotas = "<div class='gota'></div>";
        if($("gota").length > 0){
            $("gota").css({opacity:0,"animation":"100ms all"});
        }
        let porcentajeLLuvia = $scope.lluvia / $scope.lluviaMax;
        let cant_gotas = Math.round(porcentajeLLuvia * 200);
        $timeout(function(){
            $(".lluvia-box ").html("");
            for (var i = 1; i <= cant_gotas; i++) {
                 $(".lluvia-box").append(gotas);
            }
            $(".gota").each(function(){
                let random = (Math.floor(Math.random() * (10 - 1 + 1) + 1));
                $(this).css({
                    animation : `caida 0.6s linear infinite 0.${random}s`
                })
            })
        },110);
    }

    $scope.listar_jugadores = function(){
        servicio.listar_jugadores()
        .then(function(rsp){
            if(rsp.status == 1){
                $scope.jugadores = rsp.jugadores;
                $scope.lluviaMax = rsp.lluvia.max;
                $scope.crear_medidor_rio(rsp.medidor)
                $("#medidor_valor_max").html(rsp.medidor.max+" mm");
                $("#medidor_valor_min").html(rsp.medidor.min + " mm");
            }
        })
    }
    $scope.crear_medidor_rio = function(medidor){
        $scope.textoLimite = medidor.limite;
        let limite = (medidor.limite / medidor.max) * 100;
        for (var i = 10; i >= 1 ; i--) {
            let modelo = {
                texto : `${i} mm`,
                invisible : true,
                limite : false
            }
            //let limite = Math.round(Math.round((medidor.limite / medidor.max) * 100) * 0.1);

            $scope.limite = limite;
            if(i == 10){
                modelo.texto = `${medidor.max}`;
                modelo.invisible = false;
            }else if(i == limite){
                modelo.texto = ``;
                modelo.invisible = true;
                modelo.limite = true;
            }else if(i == 1){
                modelo.texto = `${medidor.min}`;
                modelo.invisible = false;
            }
            $scope.medidor.push(modelo);
        }
    }
    $scope.actualizar_info = function(idJugador){
        servicio.actualizar_info(idJugador)
        .then(function(rsp){
           if(rsp.status == 1){
                $scope.aportarBtn = false;
                $scope.noAportar = false;
                $scope.turno = rsp.data.turno.nombre;
                $scope.idTurno = rsp.data.turno.id;
                $scope.valReforestar = rsp.data.reforestar;
                $scope.balance = rsp.data.balance;
                $scope.premio = rsp.data.premio;
                $scope.jugadorPosicion = rsp.data.posicion;
                $scope.caudal = rsp.data.caudal;
                $scope.tieneAumento = rsp.data.aumento;
                $scope.lluviaMax = rsp.data.lluvia_max;
                $scope.lluvia = rsp.data.lluvia;
                $scope.animar_lluvia();
                $timeout(function(){
                    //$scope.actualizar_info(idJugador);
                },3000);
           }else{
                $scope.actualizar_info(idJugador);
           }
        })
    }

    $scope.verificar_estado_del_juego = function(){
        servicio.verificar_estado_juego()
        .then(function(rsp){
            if(rsp.status == 1){
                if(rsp.turno != $scope.turno){
                    $("#turno").modal("show");
                    $scope.actualizar_info(idJugador);
                }
                $timeout(function(){
                    $scope.verificar_estado_del_juego();
                },3000);
            }else if(rsp.status == 2){
                if($scope.fin_juego == false){
                    $scope.fin_juego = true;
                    console.log(rsp.status)
                    $("#fin_juego").modal("show");
                }
            }else{
                $scope.verificar_estado_del_juego();
            }
        })
    }
    $scope.iniciar_juego = function(){
        $scope.listar_jugadores();
        $scope.verificar_estado_del_juego();
        $(".capa-inicio").removeClass("js-capa-active");
        $(".capa-inicio").addClass("js-capa-inactive");
        $(".js-capa-juego").removeClass("js-capa-inactive");
        $(".js-capa-juego").addClass("js-capa-active");
    }

    $scope.investigar = function(){
        servicio.investigar(idJugador)
        .then(function(rsp){
            if(rsp.status == 1){
                $scope.riego = true
            }else if(rsp.status == 0){
                let aviso = $('#aviso_comun')
                aviso.find('.modal-body').html(`<span>${rsp.mensaje}</span>`)
                aviso.modal('show')
            }
        })
    }
    $scope.aportar = function(aportar){
        if(aportar==true){
            if(!$scope.noAportar){
                $scope.aportarBtn = true;
            }
        }else{
            if(!$scope.aportarBtn){
                $scope.noAportar = true;
            }
        }
        let val_aportar = aportar == true ? 1 : 0;
        servicio.aportar(idJugador,val_aportar)
        .then(function(rsp){
            
        })
    }
    $scope.animar_lluvia();
}])
