function dashboard(){
	console.log("inicio");
	var service = new Service("");
	var appAngular = angular.module('dashboard',[]);
	var data;
	appAngular.controller('seccionDecisiones', 
	    function($scope,$http){
	    console.log("seccionDecisiones");
	    $scope.data = {};
	    $(window).on("cargoData",function(){
	    	$scope.data = data;
	    	$scope.$digest();
	    });
	});

	appAngular.controller('seccionBalance', 
	    function($scope,$http){
	    console.log("seccionBalance");
	    $scope.data = {};
	    $(window).on("cargoData",function(){
	    	$scope.data = data;
	    	$scope.$digest();
	    });
	});
	$(".botonSeccion").on("click",function(){
		var elemento = $(this);
		var seccion = elemento.data("seccion");
		if(seccion == "d"){
			$(".opcional").show();
			$("#balance").fadeOut(function(){
				$("#decisiones").fadeIn();
			});
		}
		else{
			$(".opcional").hide();
			$("#decisiones").fadeOut(function(){
				$("#balance").fadeIn();
			});
		}
		$(".botonSeccion").removeClass("activo");
		$(this).addClass("activo");
		consultarData();
	});

	function init(){
		consultarData();
	}
	function consultarData(){
		var consulta = service.consultar("app/data");
		consulta.then(function(evt){
			console.log("inicio",evt);
			data = evt;
			$(window).trigger("cargoData");
		},function(err){
			console.log("err",err);
		} );
	}
	init();
}	
dashboard();