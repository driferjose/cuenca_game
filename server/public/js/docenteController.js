appAngular.controller('docenteController',['$scope',"$http","$timeout","servicio", 
    function($scope,$http,$timeout,servicio){
    console.log("test")

    $scope.config = {};
    $scope.jugadores;
    $scope.jugadores_checklist;
    $scope.datos_jugador = {};
    $scope.idTurno = 0;
    $scope.lluvia = 0;
    $scope.turno = "";
    $scope.valReforestar = 0;
    $scope.caudal = 0;
    $scope.juegoPausa = false;
    $scope.nextTurno = false;
    $scope.aportar = false;
    $scope.medidor = [];
    $scope.btnReforestar = false;
    $scope.lluviaMax = 0;
    $scope.limite = 0;
    $scope.textoLimite = "";
    
    $scope.reforestar = function(){
        if($scope.btnReforestar == false){
            $scope.btnReforestar = true;
        }else{
            $scope.btnReforestar = false;
        }
    }
    $scope.crear_medidor_rio = function(medidor){
        $scope.textoLimite = medidor.limite;
        let limite = (medidor.limite / medidor.max) * 100;
        console.log(limite)
        for (var i = 10; i >= 1 ; i--) {
            let modelo = {
                texto : `${i} mm`,
                invisible : true,
                limite : false
            }
            //let limite = Math.round(Math.round((medidor.limite / medidor.max) * 100) * 0.1);

            $scope.limite = limite;
            if(i == 10){
                modelo.texto = `${medidor.max}`;
                modelo.invisible = false;
            }else if(i == limite){
                modelo.texto = ``;
                modelo.invisible = true;
                modelo.limite = true;
            }else if(i == 1){
                modelo.texto = `${medidor.min}`;
                modelo.invisible = false;
            }
            $scope.medidor.push(modelo);
        }
    }
    $scope.obtener_configuracion = function(){
        servicio.obtener_configuracion()
        .then(function(datos){
            $scope.config = datos;
        })
    }
    $scope.guardar_configuracion = function(){
        servicio.configurar_juego($scope.config)
        .then(function(rsp){

        })
    }
    $scope.mostrar_modal_configuracion = function(){
        $("#config_modal").modal("show");
    }
    $scope.siguiente_turno = function(){
        let path = $scope.btnReforestar == true ? 1 : 0;
        servicio.siguiente_turno(path)
        .then(function(rsp){
            if(rsp.status == 1){
                $scope.nextTurno = false;
                $scope.btnReforestar = false;
                $scope.lluvia = rsp.data.lluvia;
                $scope.valReforestar = rsp.data.reforestacion;
                $scope.caudal = rsp.data.caudal;
                $scope.turno = rsp.data.turno;
                $scope.animar_lluvia();
                if(rsp.finJuego){
                    $("#fin_juego").modal("show");
                }
            }
        })
    }
    $scope.mostrar_pregunta_reforestar = function(){
        $("#modal_reforestar").modal("show");
        if($scope.nextTurno == false){
            $scope.nextTurno = true;
        }else{
            $scope.nextTurno = false;
        }
    }
    $scope.validar_si_hay_idJuego = function(){
        if(typeof idJuego != 'undefined'){
            if( idJuego > 0){
                $scope.listar_jugadores(idJuego);
                $scope.obtener_jugadores_actualizados();
            }
        }
    }
    $scope.mostrar_modal = function(titulo,texto){
        let padre = $("#modal_mensaje");
        padre.find(".modal-title").html(titulo);
        padre.find(".modal-body p").html(texto);
        padre.modal("show");
    }
    $scope.enviar_premio = function(){
        servicio.enviar_premio()
        .then(function(rsp){

        })
    }
    $scope.pausar_turno = function(){
        if($scope.juegoPausa == false){
            $scope.juegoPausa = true;
        }else{
            $scope.juegoPausa = false
        }
        servicio.pausar_turno()
        .then(function(rsp){
            console.log("turno pausado");
        })
    }
    $scope.actualizar_jugadores = function(){
        servicio.actualizar_jugadores()
        .then(function(rsp){
            //console.log("resss",rsp);
           if(rsp.status == 1){
                $scope.jugadores = rsp.data.jugadores;
                $timeout($scope.actualizar_jugadores,3000);
           }
           else{
                $scope.actualizar_jugadores();
           }
        },function(err){
            console.log("sdsd",err);
        })
        .catch(function(error){
            console.log("El error",error)
        })
        
    }
    $scope.animar_lluvia = function(){
        let gotas = "<div class='gota'></div>";
        if($("gota").length > 0){
            $("gota").css({opacity:0,"animation":"100ms all"});
        }
        let porcentajeLLuvia = $scope.lluvia / $scope.lluviaMax;
        let cant_gotas = Math.round(porcentajeLLuvia * 200);
        $timeout(function(){
            $(".lluvia-box ").html("");
            for (var i = 1; i <= cant_gotas; i++) {
                 $(".lluvia-box").append(gotas);
            }
            $(".gota").each(function(){
                let random = (Math.floor(Math.random() * (10 - 1 + 1) + 1));
                $(this).css({
                    animation : `caida 0.6s linear infinite 0.${random}s`
                })
            })
        },110);
    }
    $scope.iniciar_juego = function(){
        let data = [];
        servicio.iniciar_juego(data)
        .then(function(rsp){
            if(rsp.status == 1){
                $scope.lluvia = rsp.data.lluvia.valor;
                $scope.lluviaMax = rsp.data.lluvia.max;
                $scope.valReforestar = rsp.data.reforestacion;
                $scope.caudal = rsp.data.caudal;
                $scope.turno = rsp.data.turno.nombre;
                $scope.animar_lluvia();
                $scope.crear_medidor_rio(rsp.data.medidor);
                $scope.actualizar_jugadores();
                $(".capa-inicio").removeClass("js-capa-active");
                $(".capa-inicio").addClass("js-capa-inactive");
                $(".js-capa-juego").removeClass("js-capa-inactive");
                $(".js-capa-juego").addClass("js-capa-active");
            }
        })
    };
    $scope.obtener_configuracion();
    //$scope.actualizar_jugadores();
}])
