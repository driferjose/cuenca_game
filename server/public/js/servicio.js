angular.module('simulador.servicios')
.factory('servicio',['$rootScope',"$http","$timeout","$q", function($scope,$http,$timeout,$q){
    return {
        iniciar_juego : function(data){
            var deferred = $q.defer();
            $http({
                url : `${host}/docente/init-juego`,
                method : "POST",
                headers:{
                   'Content-Type': 'application/json'
                },
                data : data
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        },
         configurar_juego : function(data){
            var deferred = $q.defer();
            $http({
                url : `${host}/docente/configurar-juego`,
                method : "POST",
                headers:{
                   'Content-Type': 'application/json'
                },
                data : data
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        },
        actualizar_jugadores : function(){
            var deferred = $q.defer();
            $http({
                url : `${host}/docente/actualizar-jugadores`,
                method : "GET",
                headers:{
                   'Content-Type': 'application/json'
                }
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                console.log("errorrr");
                deferred.resolve(response);
            });
            return deferred.promise;
        },
        obtener_configuracion : function(){
            var deferred = $q.defer();
            $http({
                url : `${host}/docente/configuracion`,
                method : "GET",
                headers:{
                   'Content-Type': 'application/json'
                }
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        },
        verificar_estado_juego : function(){
            var deferred = $q.defer();
            $http({
                url : `${host}/usuario/juego-estado`,
                method : "GET",
                headers:{
                   'Content-Type': 'application/json'
                }
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        },
        actualizar_info : function(idJugador){
            var deferred = $q.defer();
            $http({
                url : `${host}/usuario/info/${idJugador}`,
                method : "GET",
                headers:{
                   'Content-Type': 'application/json'
                }
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        },
        listar_jugadores : function(){
            var deferred = $q.defer();
            $http({
                url : `${host}/usuario/listar-jugadores`,
                method : "GET",
                headers:{
                   'Content-Type': 'application/json'
                }
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        },
        aportar : function(idJugador,aportar){
            var deferred = $q.defer();
            $http({
                url : `${host}/usuario/aportar/${idJugador}/${aportar}`,
                method : "GET",
                headers:{
                   'Content-Type': 'application/json'
                },
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        },
        investigar : function(idJugador){
            var deferred = $q.defer();
            $http({
                url : `${host}/usuario/investigar/${idJugador}`,
                method : "GET",
                headers:{
                   'Content-Type': 'application/json'
                },
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        },
        enviar_premio : function(){
            var deferred = $q.defer();
            $http({
                url : `${host}/docente/premio`,
                method : "GET",
                headers:{
                   'Content-Type': 'application/json'
                }
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        },
        siguiente_turno : function(pathRefo){
            var deferred = $q.defer();
            $http({
                url : `${host}/docente/siguiente-turno/${pathRefo}`,
                method : "GET",
                headers:{
                   'Content-Type': 'application/json'
                }
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        },
        pausar_turno : function(){
            var deferred = $q.defer();
            $http({
                url : `${host}/docente/pausar-turno`,
                method : "GET",
                headers:{
                   'Content-Type': 'application/json'
                }
            }).then(function(response){
                deferred.resolve(response.data);
            },function(response){
                deferred.resolve(response);
            });
            return deferred.promise;
        }
    }
}])
