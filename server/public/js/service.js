function Service(urlBase){	
	var url = urlBase;
	
	this.consultar = function (){
		var ar = [];
		for (i = 0; i < arguments.length; i++) {
			ar.push(arguments[i]);
		}
		var urlFinal = ar.join("/");
		urlFinal = url+urlFinal;
		var conexion = new ServiceConsulta(urlFinal);
		return conexion;
	}
	this.consultarPost = function (){
		var ar = [];
		for (i = 0; i < arguments.length-1; i++) {
			ar.push(arguments[i]);
		}
		var urlFinal = ar.join("/");
		urlFinal = url+urlFinal;
		//console.log("sdsds",urlFinal);
		var conexion = new ServiceConsulta(urlFinal,"POST","json",arguments[arguments.length-1]);
		return conexion;
	}
	this.consultarPostTexto = function (){
		var ar = [];
		for (i = 0; i < arguments.length-1; i++) {
			ar.push(arguments[i]);
		}
		var urlFinal = ar.join("/");
		urlFinal = url+urlFinal;
		var conexion = new ServiceConsulta(urlFinal,"POST","text",arguments[arguments.length-1]);
		return conexion;
	}
	function errorLectura(evt,ajvac,thown){
		console.log("error texto",evt.responseText);
	}

}
function ServiceConsulta(urlConsulta,tipo,formato,data){
	if(tipo == null){
		tipo = "GET";
	}
	if(formato == null){
		formato = "json";
	}
	var instancia = $("<div>");
	$.ajax({		  
		dataType:formato,
		data:data,
		type:tipo,
		//async:false,

		url: urlConsulta,
		success: function(evt,ss,aa){
			if(instancia.f1){
				instancia.f1(evt);
			}
		},
		error:function(evt){
			console.log("error texto",evt);
			//console.log("error",evt,ajvac,thown);
			if(instancia.f2){
				instancia.f2();
			}
		}
	});
	instancia.then = function(f1,f2){
		if(f1){
			instancia.f1 = f1;
		}
		if(f2){
			instancia.f2 = f2;
		}
	}
	return instancia;
}	