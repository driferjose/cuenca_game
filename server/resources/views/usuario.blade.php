<!DOCTYPE html>
<html lang="en" ng-app="simulador">
<?php 
    $app_url = Config::get('app.url');
?>
<head>
    <meta charset="UTF-8">
    <title>Simulador | Jugador</title>
    <link rel="stylesheet" href="{{$app_url}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{$app_url}}/css/iconos.css">
    <link rel="stylesheet" href="{{$app_url}}/css/main.css">
</head>
<script>
    var idJugador = <?php echo $jugador; ?>;
    var host = "<?php echo $app_url; ?>";
</script>
<body ng-controller="jugadorController">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 p-0 d-flex justify-content-center">
                <div class="box-cuenca pos-relative">
                    <img src="{{$app_url}}/img/cuenca.png" class="img-principal" alt="cuenca">
                    <img  ng-show="caudal > 66" src="{{$app_url}}/img/rio1.png" class="pieza pos-absolute" alt="rio">
                    <img ng-show="caudal > 33 && caudal < 66" src="{{$app_url}}/img/rio2.png" class="pieza pos-absolute" alt="rio">
                    <img ng-show="caudal < 33" src="{{$app_url}}/img/rio3.png" class="pieza pos-absolute" alt="rio">
                    <img ng-show="valReforestar > 0 " src="{{$app_url}}/img/arboles.png" class="pieza pos-absolute" alt="arboles">
                    <img ng-show="lluvia > 0  " src="{{$app_url}}/img/lagunas.png" class="pieza pos-absolute" alt="lagunas">
                    <div class="js-capa capa-inicio js-capa-active pos-absolute d-flex justify-content-center align-items-center">
                        <img src="{{$app_url}}/img/logo_blanco.png"  class="pos-absolute img-logo-inicio" alt="">
                        <div class=" py-2 px-4 burbuja-descarga pos-absolute ">
                            <div>
                                <a href="{{Config::get('app.storage_path')}}/manual_usuario/manual.pdf" download class="text-white">
                                    <span class=" icon-download mr-2"></span>
                                    <span> Descargar guía de uso</span>
                                </a>
                            </div>
                        </div>
                        <div class="d-flex flex-column justify-content-center align-items-center centro-box">
                            <h1 class="text-white titulo-juego mb-0">LA CUENCA DE CAÑETE</h1>
                            <span class="txt-subtitulo text-white">
                                Simulador de Retribución por Servicios Ecosistémicos
                            </span>
                            <!-- <div class="d-flex mt-5 align-items-center c-pointer">
                                <span class="text-white js-ver-resumen">Ver resumen</span>
                                <span class="icon-circle-down text-danger mx-2"></span>
                            </div> -->
                            <div class="js-introduccion-inicio text-white w-50 mt-4">
                                <p class="text-justify">
                                    Este simulador es un juego en tiempo real donde los alumnos han de gestionar un recurso natural común: el agua del río Cañete, del cual depende la productividad de sus cultivos.
                                </p>
                                <p class="text-justify">
                                    El juego se basa en la cuenca del río Cañete situada al sur de Lima y en los procesos de Retribución por Servicios Ecosistémicos. El juego busca promover una visión integrada de la cuenca como unidad de territorio además de generar capacidades en los alumnos para la toma de decisiones frente a la gestión de un bien común.
                                </p>
                            </div>
                            <div class="btn-verde mt-5" ng-click="iniciar_juego()" >
                                <span class="h3">JUGAR</span>
                            </div>
                            <img src="{{$app_url}}/img/logo_visionario.png" class="mt-3" alt="">
                        </div>
                    </div>
                    <div class="js-capa js-capa-juego js-capa-inactive pos-absolute">
                        <div class="container-fluid">
                            <div class="row bg-white">
                                <div class="col-lg-4 d-flex flex-column py-3 ">
                                    <span class="h3 mb-0 txt-azul">LA CUENCA DE CAÑETE</span>
                                    <span class="txt-azul">
                                            Simulador de Retribución por Servicios Ecosistémicos
                                    </span>
                                </div>
                                <div class="col-lg-4 d-flex justify-content-center align-items-center"  >
                                     <img src="{{$app_url}}/img/logo_visionario.png" class="logo mr-2" alt="" class="img-fluid">
                                </div>
                                <div class="col-lg-4 d-flex justify-content-end align-items-center">
                                    <img src="{{$app_url}}/img/logo.jpg" class="logo mr-2" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div ng-class="{'oculto':lluvia == 0, 'mostrar': lluvia != 0 }" class="w-100 d-flex lluvia-box justify-content-between pos-absolute"><!-- 
                            @for($e = 0; $e < 50 ; $e++)
                            <div class="gota"></div>
                            @endfor -->
                        </div>
                        <div ng-repeat="jugador in jugadores" 
                            class="jugador jugador-@{{jugador.posicion}}  pos-absolute d-flex flex-column align-items-center">
                            <div class="numero pos-absolute d-flex justify-content-center align-items-center">
                                @{{jugador.posicion}} 
                            </div>
                            <div style="height:104px" class="jugador-info oculto mb-5 tarjeta px-4 py-2 d-flex flex-column align-items-center">
                                <div class="d-flex flex-column justify-content-end align-items-end">
                                    <div class="d-flex justify-content-between mb-2">
                                        <div ng-show="jugador.aporto == 'si'" class="icono-cuerpo p-1 bg-azul mr-1">
                                            <img src="{{$app_url}}/img/iconAportar.png" alt="">
                                        </div>
                                        <div ng-show="jugador.invirtio == 'si'" class="icono-cuerpo bg-azul mr-1">
                                            <img src="{{$app_url}}/img/iconRiegoTecnificado.png" alt="">
                                        </div>
                                        <div ng-show="jugador.premio == 'si'"  class="icono-cuerpo mr-1">
                                            <img src="{{$app_url}}/img/iconPremio.png" alt="">
                                        </div>
                                    </div>
                                    <div class="dinero-container d-flex align-items-center justify-content-between">
                                        <div class="dinero-arrow d-flex justify-content-center align-items-center p">
                                            <b class="text-white">S/.</b>
                                        </div>
                                        <span class="pr-3">
                                            <strong>S/ @{{jugador.balance}} </strong>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <img src="{{$app_url}}/img/jugador.png" class="img-jugador" alt="">
                        </div>
                        <div class="nivel-lluvia pos-absolute d-flex flex-column align-items-center">
                             <div class="lluvia pos-relative d-flex flex-column justify-content-around align-items-center "
                                
                            >
                                <span ng-repeat="item in medidor"
                                    ng-class="{
                                        'empujar-max-medidor':$index==0,
                                        'invisible':item.invisible == true
                                    }">
                                    <b ng-class="{'mover-texto':$index==0}">
                                       @{{item.texto}}
                                    </b>
                                </span>
                                <div class="limite pos-absolute w-100 d-flex justify-content-center"
                                    style="height: @{{limite}}%; max-height: 100%; background-color: transparent;bottom: 0;left: 0;z-index: 3" 
                                >
                                    <span style="color:red"> @{{textoLimite}} </span>
                                </div>
                                <div style="height: @{{caudal}}%;max-height: 100% " 
                                    ng-class="{'bg-naranja': caudal <= limite, }"
                                    class="lluvia-llenado pos-absolute" >
                                </div>
                            </div>
                            <div class="d-flex mt-2 flex-column justify-content-center align-items-center">
                                <img src="{{$app_url}}/img/nivelRio.png">
                                <span class="text-white">Caudal del Río - m3/s</span>
                            </div>
                        </div>
                        <div class="herramientas d-flex justify-content-around pos-absolute">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 d-flex align-items-center justify-content-center">
                                        <span class="text-white d-flex align-items-center px-3 mr-3 box-turno">
                                            <strong>
                                                @{{turno}}
                                            </strong>
                                        </span>
                                        <span class="text-white mr-2">Jugador @{{jugadorPosicion}}: </span>
                                        <div class="globito d-flex align-items-center m-1 pos-relative">
                                            <div ng-class="{'notificacion-active': premio > 0, 
                                            'notificacion-inactive' : premio == 0 }" class=" d-flex notificacion-inactive notificacion flex-column justify-content-center align-items-center m-1 pos-absolute">
                                                <div class="d-flex">
                                                    <div class="icono-cuerpo  mr-3">
                                                        <img src="{{$app_url}}/img/iconPremio.png" alt="">
                                                    </div>
                                                    <span class="text-muted">
                                                        <strong>
                                                            Premio ambiental: S/ @{{premio}}
                                                        </strong>
                                                    </span>
                                                </div>
                                                <div class="cuadradito"></div>
                                            </div>
                                            <div class="icono-cuerpo  mr-3">
                                                <img src="{{$app_url}}/img/iconBalance.png" alt="">
                                            </div>
                                            <span class="text-muted">
                                                <strong>
                                                    S/ @{{balance}}
                                                </strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 d-flex justify-content-center">
                                        <div class="box-decisiones d-flex justify-content-center align-items-end">
                                            <div class="w-100 mb-2 d-flex justify-content-center">
                                                <div class="d-flex flex-column c-pointer align-items-center w-50">
                                                    <div class="d-flex mx-2  flex-column align-items-center justify-content-center btn-control bg-azul" 
                                                    ng-class="{
                                                        'bg-azul':!aportarBtn,
                                                        'bg-verde': aportarBtn,
                                                        'btn-control-active' : !noAportar
                                                    }"
                                                    data-toggle="tooltip" data-placement="top" title="S/ {{$decisiones['aportar']}}"
                                                    ng-click="aportar(true)">
                                                        <img src="{{$app_url}}/img/iconAportar.png" alt="">
                                                        
                                                    </div>
                                                    <span class="text-white">Aportar</span>
                                                </div>
                                                <div class="d-flex flex-column w-50 c-pointer align-items-center">
                                                    <div class="d-flex justify-content-center flex-column align-items-center btn-control bg-azul"
                                                    ng-class="{
                                                        'bg-azul':!noAportar,
                                                        'bg-verde':noAportar,
                                                        'btn-control-active' : !aportarBtn
                                                    }"
                                                    ng-click="aportar(false)">
                                                        <img src="{{$app_url}}/img/iconNoAportar.png" alt="">
                                                    </div>
                                                    <span class="text-white">No aportar</span>
                                                </div>
                                                <div class="d-flex flex-column w-50  align-items-center">
                                                    <div class="d-flex justify-content-center btn-riego-activo flex-column align-items-center btn-control bg-azul"
                                                    data-toggle="tooltip" data-placement="top" title="S/ {{$decisiones['investigar']}}"
                                                    ng-class="{
                                                        'bg-azul':!riego,
                                                        'control-riego':riego
                                                    }"
                                                    ng-click="investigar()">
                                                        <img src="{{$app_url}}/img/iconRiegoTecnificado.png" alt="">
                                                    </div>
                                                    <span class="text-white text-center text-icono-control">Riego tecnificado</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 
                                        d-flex align-items-center 
                                        justify-content-center">
                                        <!-- <div class="globito m-1">
                                            <span class="text-muted">
                                                <strong>
                                                    @{{turno}}
                                                </strong>
                                            </span>
                                        </div> -->
                                        <div class="globito d-flex align-items-center m-1">
                                            <div class="icono-cuerpo bg-celeste mr-1">
                                                <img src="{{$app_url}}/img/iconLluvia.png" alt="">
                                            </div>
                                            <span class="text-muted">
                                                <strong>
                                                    Lluvia: @{{lluvia}} mm
                                                </strong>
                                            </span>
                                        </div>
                                        <!-- <div class="globito d-flex align-items-center m-1">
                                            <div class="icono-cuerpo bg-verde mr-1">
                                                <img src="{{$app_url}}/img/iconReforestacion.png" alt="">
                                            </div>
                                            <span class="text-muted">
                                                <strong>
                                                    Reforestación: @{{valReforestar}}
                                                </strong>
                                            </span>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MENSAJE MODAL AVISO COMUN -->
    <div class="modal fade" tabindex="-1" role="dialog" id="aviso_comun" >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-center">La cuenca de Cañete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary text-white" 
                data-dismiss="modal">Continuar</button>
            </div>    
          </div>
        </div>
      </div>
    </div>

    <!-- MENSAJE MODAL FIN DEL JUEGO -->
    <div class="modal fade" tabindex="-1" role="dialog" id="fin_juego" >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-center">¡Fin del juego!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div style="height: 20vh" class="modal-body  d-flex justify-content-center align-items-center">
            <div class="btn-azul" >
                <span>
                    <a href="{{$app_url}}/"><strong class="text-white">VER RESULTADOS</strong></a>
                </span>
            </div>      
          </div>
        </div>
      </div>
    </div>
    <!-- MENSAJE MODAL SIGUIENTE TURNO -->
    <div class="modal fade" tabindex="-1" role="dialog" id="turno" >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-center txt-azul titulo-mayuscula">@{{turno}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <!-- <span class="h3 mb-3 txt-azul">Resumen</span> -->
            <div class="w-100">
               <div class="w-100 d-flex justify-content-between">
                    <span>Balance</span>
                    <span>S/ @{{balance}} </span>
               </div>
               <div class="w-100 d-flex justify-content-between">
                   <span>Invirtió en riego tecnificado:</span>
                   <span>@{{tieneAumento == 1 ? 'Sí' : 'No'}}</span>
               </div>
               <div class="w-100 d-flex justify-content-between">
                   <span>Premio ambiental</span>
                   <span>S/ @{{premio}} </span>
               </div>
            </div>
            <div class="w-100 mt-3 d-flex justify-content-start">
                <span class="txt-verde">
                    @{{valReforestar > 0 ? 'Hay' : 'No hay'}} reforestación en este turno
                </span>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary text-white" 
                data-dismiss="modal">Continuar</button>
          </div>
        </div>
      </div>
    </div>
    <script src="{{$app_url}}/js/jquery-3.2.1.min.js"></script>
    <script src="{{$app_url}}/js/popper.min.js"></script>
    <script src="{{$app_url}}/js/bootstrap.min.js"></script>
    <script src="{{$app_url}}/js/angular.min.js"></script>
    <script src="{{$app_url}}/js/app.js"></script>
    <script src="{{$app_url}}/js/servicio.js"></script>
    <script src="{{$app_url}}/js/controllers.js"></script>
    <script src="{{$app_url}}/js/jugadorController.js"></script>
</body>
</html>