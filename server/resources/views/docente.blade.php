<!DOCTYPE html>
<html lang="en" ng-app="simulador">
<?php $app_url = Config::get('app.url'); ?>
<head>
    <meta charset="UTF-8">
    <title>Simulador | Docente</title>
    <link rel="stylesheet" href="{{$app_url}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{$app_url}}/css/iconos.css">
    <link rel="stylesheet" href="{{$app_url}}/css/main.css">
</head>
<script>
    var host = "<?php echo $app_url; ?>";
</script>
<body ng-controller="docenteController">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 p-0 d-flex justify-content-center">
                <div class="box-cuenca pos-relative">
                    <img src="{{$app_url}}/img/cuenca.png" class="img-principal" alt="cuenca">
                    <img  ng-show="caudal > 66" src="{{$app_url}}/img/rio1.png" class="pieza pos-absolute" alt="rio">
                    <img ng-show="caudal > 33 && caudal < 66" src="{{$app_url}}/img/rio2.png" class="pieza pos-absolute" alt="rio">
                    <img ng-show="caudal < 33" src="{{$app_url}}/img/rio3.png" class="pieza pos-absolute" alt="rio">
                    <img ng-show="valReforestar > 0 " src="{{$app_url}}/img/arboles.png" class="pieza pos-absolute" alt="arboles">
                    <img ng-show="lluvia > 0  " src="{{$app_url}}/img/lagunas.png" class="pieza pos-absolute" alt="lagunas">
                    <div class="js-capa capa-inicio js-capa-active pos-absolute d-flex justify-content-center align-items-center">
                        <img src="{{$app_url}}/img/logo_blanco.png"  class="pos-absolute img-logo-inicio" alt="">
                        <div class=" py-2 px-4 burbuja-descarga pos-absolute ">
                                <div>
                                    <a href="{{Config::get('app.storage_path')}}/manual_usuario/manual.pdf" download class="text-white">
                                        <span class=" icon-download mr-2"></span>
                                        <span> Descargar guía de uso</span>
                                    </a>
                                </div>
                            </div>
                        <div class="d-flex flex-column justify-content-center align-items-center centro-box">
                            <h1 class="text-white titulo-juego mb-0 mt-3">LA CUENCA DE CAÑETE</h1>
                            <span class="text-white txt-subtitulo">Simulador de Retribución por Servicios Ecosistémicos</span>
                           <!--  <div class="d-flex mt-5 align-items-center c-pointer">
                                <span class="text-white js-ver-resumen">Ver resumen</span>
                                <span class="icon-circle-down text-danger mx-2"></span>
                            </div> -->
                            <div class="js-introduccion-inicio text-white w-50 mt-4">
                                <p class="text-justify">
                                    Este simulador es un juego en tiempo real donde los alumnos han de gestionar un recurso natural común: el agua del río Cañete, del cual depende la productividad de sus cultivos.
                                </p>
                                <p class="text-justify">
                                    El juego se basa en la cuenca del río Cañete situada al sur de Lima y en los procesos de Retribución por Servicios Ecosistémicos. El juego busca promover una visión integrada de la cuenca como unidad de territorio además de generar capacidades en los alumnos para la toma de decisiones frente a la gestión de un bien común.
                                </p>
                            </div>
                             <div class="btn-config mt-5" ng-click="mostrar_modal_configuracion()" >
                                <span class="h4">OPCIONES</span>
                            </div>
                            <div class="btn-verde mt-3" ng-click="iniciar_juego()" >
                                <span class="h4">JUGAR</span>
                            </div>
                            <img src="{{$app_url}}/img/logo_visionario.png" class="mt-3" alt="">
                        </div>
                    </div>
                    <div class="js-capa js-capa-juego js-capa-inactive pos-absolute">
                        <div class="container-fluid">
                            <div class="row bg-white">
                                <div class="col-lg-4 d-flex flex-column py-3 ">
                                    <span class="h3 mb-0 txt-azul">LA CUENCA DE CAÑETE</span>
                                    <span class="txt-azul ">
                                            Simulador de Retribución por Servicios Ecosistémicos
                                    </span>
                                </div>
                                <div class="col-lg-4 d-flex justify-content-center align-items-center"  >
                                     <img src="{{$app_url}}/img/logo_visionario.png" class="logo mr-2" alt="" class="img-fluid">
                                </div>
                                <div class="col-lg-4 d-flex justify-content-end align-items-center">
                                    <img src="{{$app_url}}/img/logo.jpg" class="logo mr-2" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div ng-class="{'oculto':lluvia == 0, 'mostrar': lluvia != 0 }" class="w-100 d-flex lluvia-box justify-content-between pos-absolute">
                            
                        </div>
                        <div ng-repeat="jugador in jugadores" 
                            class="jugador jugador-@{{jugador.posicion}}  pos-absolute d-flex flex-column align-items-center">
                            <div class="numero pos-absolute d-flex justify-content-center align-items-center">
                                @{{jugador.posicion}} 
                            </div>
                            <div class="jugador-info mb-5 tarjeta px-4 py-2 d-flex flex-column align-items-center">
                                <div class="d-flex flex-column justify-content-end align-items-end">
                                    <div class="d-flex justify-content-between mb-2">
                                        <div ng-show="jugador.aporto == 'si'" class="icono-cuerpo p-1 mr-1">
                                            <img src="{{$app_url}}/img/d2.png" alt="">
                                        </div>
                                        <div ng-show="jugador.noAporto == 'si'" 
                                            class="icono-cuerpo p-1 mr-1">
                                            <img src="{{$app_url}}/img/d3.png" alt="">
                                        </div>
                                        <div ng-show="jugador.invirtio == 'si'" class="icono-cuerpo mr-1">
                                            <img src="{{$app_url}}/img/d1.png" alt="">
                                        </div>
                                        <div ng-show="jugador.premio == 'si'"  class="icono-cuerpo mr-1">
                                            <img src="{{$app_url}}/img/iconPremio.png" alt="">
                                        </div>
                                    </div>
                                    <div class="dinero-container d-flex align-items-center justify-content-between">
                                        <div class="dinero-arrow mr-2 d-flex justify-content-center align-items-center p">
                                            <b class="text-white">S/.</b>
                                        </div>
                                        <span class="pr-3">
                                            <strong> @{{jugador.balance}} </strong>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <img src="{{$app_url}}/img/jugador.png" class="img-jugador" alt="">
                        </div>
                        <div class="nivel-lluvia pos-absolute d-flex flex-column align-items-center">
                             <div class="lluvia pos-relative d-flex flex-column justify-content-around align-items-center "
                                
                            >
                                 <span ng-repeat="item in medidor"
                                    ng-class="{
                                        'empujar-max-medidor':$index==0,
                                        'invisible':item.invisible == true
                                    }">
                                    <b ng-class="{'mover-texto':$index==0}">
                                       @{{item.texto}}
                                    </b>
                                </span>
                                <div class="limite pos-absolute w-100 d-flex justify-content-center"
                                    style="height: @{{limite}}%; max-height: 100%; background-color: transparent;bottom: 0;left: 0;z-index: 3" 
                                >
                                    <span style="color:red"> @{{textoLimite}} </span>
                                </div>
                                <div style="height: @{{caudal}}%;max-height: 100% " 
                                    ng-class="{'bg-naranja': caudal <= limite, }"
                                    class="lluvia-llenado pos-absolute" >
                                </div>
                            </div>
                            <div class="d-flex mt-2 flex-column justify-content-center align-items-center">
                                <img src="{{$app_url}}/img/nivelRio.png">
                                <span class="text-white">Caudal del Río - m3/s</span>
                            </div>
                        </div>
                        <div class="herramientas d-flex justify-content-around pos-absolute">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 d-flex align-items-center justify-content-center">
                                        <span class="text-white d-flex align-items-center px-3 box-turno">
                                            <strong>
                                                @{{turno}}
                                            </strong>
                                        </span>
                                        <div class="btn-azul ml-3  " ng-click="enviar_premio()">
                                            <img src="{{$app_url}}/img/iconPremio_blanco.png" alt="" class="mr-2">
                                            <span>
                                                <strong>
                                                    Premio ambiental
                                                </strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 d-flex justify-content-center">
                                        <div class="box-decisiones d-flex justify-content-center align-items-end">
                                            <div class="w-100 mb-2 d-flex justify-content-center">
                                                <div class="d-flex flex-column w-50 c-pointer align-items-center">
                                                    <div class="d-flex justify-content-center flex-column btn-control-active align-items-center btn-control bg-azul"
                                                    ng-class="{'bg-azul':!btnReforestar,'bg-verde':btnReforestar}"
                                                    ng-click="reforestar()">
                                                        <img style="transform:scale(1.3)" src="{{$app_url}}/img/iconReforestacion.png" alt="">
                                                    </div>
                                                    <span class="text-white">Reforestación</span>
                                                </div>
                                                <!-- <div class="d-flex flex-column c-pointer align-items-center w-50">
                                                    <div class="d-flex mx-2  flex-column btn-control-active align-items-center justify-content-center btn-control bg-azul"
                                                    ng-click="pausar_turno()" 
                                                    ng-class="{'bg-azul':!juegoPausa,'bg-verde':juegoPausa}">
                                                        <img src="{{$app_url}}/img/iconPausa.png" alt="">
                                                        
                                                    </div>
                                                    <span class="text-white">Pausa</span>
                                                </div> -->
                                                <div class="d-flex flex-column w-50 c-pointer align-items-center">
                                                    <div class="d-flex justify-content-center btn-control-active btn-control-active flex-column align-items-center btn-control bg-azul"
                                                    ng-class="{'bg-azul':!nextTurno,'bg-verde':nextTurno}"
                                                    ng-click="siguiente_turno()">
                                                        <img src="{{$app_url}}/img/iconSgteTurno.png" alt="">
                                                    </div>
                                                    <span class="text-white">Siguiente turno</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 
                                        d-flex align-items-center 
                                        justify-content-center">
                                        <!-- <div class="globito m-1">
                                            <span class="text-muted">
                                                <strong>
                                                    @{{turno}}
                                                </strong>
                                            </span>
                                        </div> -->
                                        <div class="globito d-flex align-items-center m-1">
                                            <div class="icono-cuerpo bg-celeste mr-1">
                                                <img src="{{$app_url}}/img/iconLluvia.png" alt="">
                                            </div>
                                            <span class="text-muted">
                                                <strong>
                                                    Lluvia: @{{lluvia}} mm
                                                </strong>
                                            </span>
                                        </div>
                                        <!-- <div class="globito d-flex align-items-center m-1">
                                            <div class="icono-cuerpo bg-verde mr-1">
                                                <img src="{{$app_url}}/img/iconReforestacion.png" alt="">
                                            </div>
                                            <span class="text-muted">
                                                <strong>
                                                    Reforestación: @{{valReforestar > 0 ? 'Activo' : 'Inactivo' }}
                                                </strong>
                                            </span>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CONFIGURACION MODAL -->
    <div class="modal fade" id="config_modal" tabindex="-1" role="dialog" aria-labelledby="config_modal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">OPCIONES</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body modal-config-body">
            <form>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Balance inicial (S/.):</label>
                <input type="number" ng-model="config.juego.balanceInicial" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Premio ambiental (S/.):</label>
                <input type="number" ng-model="config.premio" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Aumento/reducción del caudal por aporte (m3/s):</label>
                <input type="number" ng-model="config.juego.varianteCuadal" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Balance inicial (S/.):</label>
                <input type="number" ng-model="config.juego.balanceInicial" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Costo de Aporte (S/.):</label>
                <input type="number" ng-model="config.aporte" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Producción anual (kg):</label>
                <input type="number" ng-model="config.juego.produccionKg" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Precio por kg (S/.):</label>
                <input type="number" ng-model="config.juego.precioPorKg" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Costo fijo mensual (S/.):</label>
                <input type="number" ng-model="config.juego.costoFijo" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Costo de Investigación y desarrollo (S/.):</label>
                <input type="number" ng-model="config.investigacion" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Impacto al caudal por reforestación (m3/s):</label>
                <input type="number" ng-model="config.reforestacion" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Porcentaje penalidad en producción (%):</label>
                <input type="number" ng-model="config.juego.porcentajePenalidad" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Porcentaje de aumento en I&E (%):</label>
                <input type="number" ng-model="config.juego.porcentajeAumentoIE" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Caudal límite (m3/s): </label>
                <input type="number" ng-model="config.juego.caudalLimite" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Caudal Inicial (m3/s) : </label>
                <input type="number" ng-model="config.juego.caudalFinal" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Max Caudal (m3/s): </label>
                <input type="number" ng-model="config.juego.maxCaudalInt" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Min Caudal (m3/s): </label>
                <input type="number" ng-model="config.juego.minCaudalInt" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Max lluvia (m3/s): </label>
                <input type="number" ng-model="config.juego.maxLluvia" class="form-control" id="recipient-name">
              </div>
              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Min lluvia (m3/s): </label>
                <input type="number" ng-model="config.juego.minLluvia" class="form-control" id="recipient-name">
              </div>
              <span class="h4">Configuración de lluvias (m3/s): </span>
              <div class="form-group" ng-repeat="turno in config.turnos">
                <label for="recipient-name" class="col-form-label"> Lluvia en @{{turno.nombre}} 
                <input type="number" ng-model="turno.lluvia" class="form-control" id="recipient-name">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="guardar_configuracion()" >Guardar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- MENSAJE MODAL -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_reforestar" >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">REFORESTACIÓN</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>¿Quieres reforestar en el siguiente turno?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary text-white"
                ng-click="siguiente_turno(true)"
                 data-dismiss="modal">Si</button>
            <button type="button" class="btn btn-secondary text-white" 
                ng-click="siguiente_turno(false)"
                data-dismiss="modal">No</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- MENSAJE MODAL FIN DEL JUEGO -->
    <div class="modal fade" tabindex="-1" role="dialog" id="fin_juego" >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-center">¡Fin del juego!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div style="height: 20vh" class="modal-body  d-flex justify-content-center align-items-center">
            <div class="btn-azul" >
                <a href="">
                    <span>
                        <a href="{{$app_url}}/"><strong class="text-white">VER RESULTADOS</strong></a>
                    </span>
                </a>
            </div>      
          </div>
        </div>
      </div>
    </div>
    <script src="{{$app_url}}/js/jquery-3.2.1.min.js"></script>
    <script src="{{$app_url}}/js/popper.min.js"></script>
    <script src="{{$app_url}}/js/bootstrap.min.js"></script>
    <script src="{{$app_url}}/js/angular.min.js"></script>
    <script src="{{$app_url}}/js/app.js"></script>
    <script src="{{$app_url}}/js/servicio.js"></script>
    <script src="{{$app_url}}/js/controllers.js"></script>
    <script src="{{$app_url}}/js/docenteController.js"></script>
</body>
</html>