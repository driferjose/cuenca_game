<!DOCTYPE html>
<html>
<head>
	<title>
		Home
	</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/app.css">
</head>
<body ng-app="dashboard">
	<div class="container-fluid">
		<div class="row cabecera">
			<div class="col-md-6">
				<figure>
					<img src="img/cuenca_texto.png">
				</figure>
			</div>
			<div class="col-md-6 text-right">
				<figure>
					<img src="img/logo.jpg">
				</figure>
			</div>
		</div>
		<div class="barra">
			<h1>
				REGISTRO DE DATOS
			</h1>
		</div>
		<div class="row filtros">
			<div class="col-md-2 activo botonSeccion" data-seccion="d">
				<label >
					Decisiones
				</label>
			</div>
			<div class="col-md-2 botonSeccion" data-seccion="b">
				<label >
					Balance
				</label>
			</div>
			<div class="col-md-8">
				<ul class="leyenda">
					
					
					
					<li class="opcional">
						<i class="fa fa-circle rosado" aria-hidden="true"></i>
						Aportó
					</li>
					<li class="opcional">
						<i class="fa fa-circle plomo" aria-hidden="true"></i>
						No aportó
					</li>
					<li>
						<i class="fa fa-circle verde" aria-hidden="true"></i>
						Reforestación
					</li>
					<li>
						<i class="fa fa-circle azul" aria-hidden="true"></i>
						Lluvia
					</li>
				</ul>
			</div>
		</div>
		<div id="decisiones" class="seccion table-responsive" ng-controller="seccionDecisiones">
			<table class="table">
				<thead>
					<tr>
						<th>
							<label>
								Jugadores
							</label>
						</th>
						<th ng-repeat="turno in data.arrayTurnos">
							<label>
								@{{turno.pos}} <i class="fa fa-tree" ng-class="{'verde':turno.refos>0,'plomo':turno.refos<=0}" aria-hidden="true"></i>		
							</label>
							<i class="fa fa-circle azul" aria-hidden="true"></i> @{{turno.lluvia}} mm 
							
											
						</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="jugador in data.arrayData">
						
						<td>
							<span class="numeracion">@{{jugador.pos}}</span>
						</td>
						<td ng-repeat="turno in jugador.array">
							<i class="fa fa-money " ng-class="{'rosado':turno.colaboro,'plomo':!turno.colaboro}" aria-hidden="true"></i>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="balance" class="seccion table-responsive" ng-controller="seccionBalance">
			<table class="table">
				<thead>
					<tr>
						<th>
							<label>
								Jugadores
							</label>
						</th>
						<th ng-repeat="turno in data.arrayTurnos">
							<label>
								@{{turno.pos}}<i class="fa fa-tree" ng-class="{'verde':turno.refos>0,'plomo':turno.refos<=0}" aria-hidden="true"></i>	
							</label>
							<i class="fa fa-circle azul" aria-hidden="true"></i> @{{turno.lluvia}} mm
						</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="jugador in data.arrayData">
						
						<td>
							<span class="numeracion">@{{jugador.pos}}</span>
						</td>
						<td ng-repeat="turno in jugador.array">
							<span class="bono">
								@{{turno.bono}}
							</span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>

	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/angular.min.js"></script>
	<script type="text/javascript" src="js/service.js"></script>
	<script type="text/javascript" src="js/dashboard.js"></script>
</body>
</html>