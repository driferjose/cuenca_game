-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.26-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla db_simulador_cuenca.decision
DROP TABLE IF EXISTS `decision`;
CREATE TABLE IF NOT EXISTS `decision` (
  `idDecision` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estado` enum('activo','inactivo') COLLATE utf8_unicode_ci NOT NULL,
  `unSoloUso` enum('si','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idDecision`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.decision: ~5 rows (aproximadamente)
DELETE FROM `decision`;
/*!40000 ALTER TABLE `decision` DISABLE KEYS */;
INSERT INTO `decision` (`idDecision`, `descripcion`, `cantidad`, `estado`, `unSoloUso`, `created_at`, `updated_at`) VALUES
	(1, 'Aportar', 1000, 'activo', 'no', NULL, NULL),
	(2, 'Investigación y Desarrollo', 3000, 'activo', 'si', NULL, NULL),
	(3, 'Reforestación', 200, 'activo', 'no', NULL, NULL),
	(4, 'Premio', 5000, 'activo', 'no', NULL, NULL),
	(5, 'Lluvia', 0, 'activo', 'no', NULL, NULL);
/*!40000 ALTER TABLE `decision` ENABLE KEYS */;

-- Volcando estructura para tabla db_simulador_cuenca.historial_balance
DROP TABLE IF EXISTS `historial_balance`;
CREATE TABLE IF NOT EXISTS `historial_balance` (
  `idHistorialBalance` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idJugador` int(11) DEFAULT NULL,
  `idTurno` int(11) DEFAULT NULL,
  `idJuego` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `balance` double DEFAULT NULL,
  PRIMARY KEY (`idHistorialBalance`)
) ENGINE=InnoDB AUTO_INCREMENT=1169 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.historial_balance: ~4 rows (aproximadamente)
DELETE FROM `historial_balance`;
/*!40000 ALTER TABLE `historial_balance` DISABLE KEYS */;
INSERT INTO `historial_balance` (`idHistorialBalance`, `idJugador`, `idTurno`, `idJuego`, `created_at`, `updated_at`, `balance`) VALUES
	(1165, 1, 1, 1, '2017-12-12 23:20:15', '2017-12-12 23:20:15', 50000),
	(1166, 2, 1, 1, '2017-12-12 23:20:15', '2017-12-12 23:20:15', 50000),
	(1167, 3, 1, 1, '2017-12-12 23:20:15', '2017-12-12 23:20:15', 50000),
	(1168, 4, 1, 1, '2017-12-12 23:20:15', '2017-12-12 23:20:15', 50000);
/*!40000 ALTER TABLE `historial_balance` ENABLE KEYS */;

-- Volcando estructura para tabla db_simulador_cuenca.juego
DROP TABLE IF EXISTS `juego`;
CREATE TABLE IF NOT EXISTS `juego` (
  `idJuego` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `balanceInicial` int(11) DEFAULT NULL,
  `caudalLimite` int(11) DEFAULT NULL,
  `caudalIntermedio` int(11) DEFAULT NULL,
  `maxCaudalInt` int(11) DEFAULT NULL,
  `minCaudalInt` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `maxLluvia` int(11) DEFAULT NULL,
  `minLluvia` int(11) DEFAULT NULL,
  `estado` enum('iniciado','finalizado') COLLATE utf8_unicode_ci NOT NULL,
  `caudalFinal` int(11) DEFAULT NULL,
  `ingreso` double DEFAULT NULL,
  PRIMARY KEY (`idJuego`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.juego: ~1 rows (aproximadamente)
DELETE FROM `juego`;
/*!40000 ALTER TABLE `juego` DISABLE KEYS */;
INSERT INTO `juego` (`idJuego`, `balanceInicial`, `caudalLimite`, `caudalIntermedio`, `maxCaudalInt`, `minCaudalInt`, `created_at`, `updated_at`, `maxLluvia`, `minLluvia`, `estado`, `caudalFinal`, `ingreso`) VALUES
	(1, 50000, 50, 23, 600, 0, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 400, 0, 'iniciado', 23, 3000);
/*!40000 ALTER TABLE `juego` ENABLE KEYS */;

-- Volcando estructura para tabla db_simulador_cuenca.jugador
DROP TABLE IF EXISTS `jugador`;
CREATE TABLE IF NOT EXISTS `jugador` (
  `idJugador` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idJuego` int(11) DEFAULT NULL,
  `idRol` int(11) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `balance` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `caudalIntermedio` double DEFAULT NULL,
  PRIMARY KEY (`idJugador`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.jugador: ~4 rows (aproximadamente)
DELETE FROM `jugador`;
/*!40000 ALTER TABLE `jugador` DISABLE KEYS */;
INSERT INTO `jugador` (`idJugador`, `idJuego`, `idRol`, `posicion`, `balance`, `created_at`, `updated_at`, `caudalIntermedio`) VALUES
	(1, 1, 2, 1, 50000, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 2),
	(2, 1, 2, 2, 50000, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 102),
	(3, 1, 2, 3, 50000, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 102),
	(4, 1, 1, 4, 50000, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 102);
/*!40000 ALTER TABLE `jugador` ENABLE KEYS */;

-- Volcando estructura para tabla db_simulador_cuenca.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.migrations: ~27 rows (aproximadamente)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2017_11_27_205236_crearTablaJuego', 1),
	('2017_11_27_205832_crearTablaJugador', 1),
	('2017_11_27_205938_crearTablaRol', 1),
	('2017_11_27_210531_crearTablaTurnoDecision', 1),
	('2017_11_27_211010_crearTablaTurno', 1),
	('2017_11_27_211543_crearTablaRolDecision', 1),
	('2017_11_27_211928_crearTablaDecision', 1),
	('2017_11_27_212340_crearTablaSetupTurno', 1),
	('2017_11_28_155311_AddColumnPoisicionToJugador', 2),
	('2017_11_28_162246_addColumnEstadoToRolDecision', 3),
	('2017_11_28_172518_addColumnTimeStampsToJuego', 4),
	('2017_11_28_174229_dropColumnsAndAddColumnsToSetupTurno', 5),
	('2017_11_28_174641_AddColumnsToJuego', 5),
	('2017_11_28_175248_EditSetuTurno', 6),
	('2017_11_28_193259_addColumnLluviatoTurno', 7),
	('2017_11_28_194047_addColumnEstadoToTurno', 8),
	('2017_11_28_205755_AddColumnEstadoToJuego', 9),
	('2017_11_28_215747_AddColumnsToJugador', 10),
	('2017_11_28_221828_AddColumnCaudalFinal', 10),
	('2017_11_28_223450_AddColumnsTimeStampsToJugador', 10),
	('2017_11_29_174836_CrearTablaPremio', 11),
	('2017_11_30_194931_addColumnCaudalIntermedioToJugador', 12),
	('2017_12_01_142145_AddColumnCaudalFinalToTurno', 13),
	('2017_12_01_154936_AddCaudalIntermedioToTurno', 14),
	('2017_12_01_155430_DropColumnCaudalIntermedioToTurno', 15),
	('2017_12_01_212646_ModifyEnumToTurno', 16),
	('2017_12_01_212917_AddColumnEstadoToTurno2', 17),
	('2017_12_04_221630_addColumnCantidadToPremio', 18),
	('2017_12_07_165137_AddColumnCaudalIntermedioToTurno', 19),
	('2017_12_11_154159_CreateTableHistorialBalance', 20),
	('2017_12_11_154617_AddColumnBalanceToHistorialBalance', 21),
	('2017_12_12_214256_AddColumnIngresoToJuego', 22);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla db_simulador_cuenca.premio
DROP TABLE IF EXISTS `premio`;
CREATE TABLE IF NOT EXISTS `premio` (
  `idPremio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idJugador` int(11) NOT NULL,
  `idJuego` int(11) DEFAULT NULL,
  `idTurno` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `valor` double DEFAULT NULL,
  PRIMARY KEY (`idPremio`),
  UNIQUE KEY `premio_idjugador_unique` (`idJugador`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.premio: ~0 rows (aproximadamente)
DELETE FROM `premio`;
/*!40000 ALTER TABLE `premio` DISABLE KEYS */;
/*!40000 ALTER TABLE `premio` ENABLE KEYS */;

-- Volcando estructura para tabla db_simulador_cuenca.rol
DROP TABLE IF EXISTS `rol`;
CREATE TABLE IF NOT EXISTS `rol` (
  `idRol` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` enum('activo','inactivo') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.rol: ~2 rows (aproximadamente)
DELETE FROM `rol`;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` (`idRol`, `descripcion`, `estado`, `created_at`, `updated_at`) VALUES
	(1, 'Docente', 'activo', NULL, NULL),
	(2, 'Usuario', 'activo', NULL, NULL);
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;

-- Volcando estructura para tabla db_simulador_cuenca.rol_decision
DROP TABLE IF EXISTS `rol_decision`;
CREATE TABLE IF NOT EXISTS `rol_decision` (
  `idRol` int(11) NOT NULL,
  `idDecision` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado` enum('activo','inactivo') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idRol`,`idDecision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.rol_decision: ~0 rows (aproximadamente)
DELETE FROM `rol_decision`;
/*!40000 ALTER TABLE `rol_decision` DISABLE KEYS */;
/*!40000 ALTER TABLE `rol_decision` ENABLE KEYS */;

-- Volcando estructura para tabla db_simulador_cuenca.setup_turno
DROP TABLE IF EXISTS `setup_turno`;
CREATE TABLE IF NOT EXISTS `setup_turno` (
  `idSetupTurno` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lluvia` int(11) DEFAULT NULL,
  PRIMARY KEY (`idSetupTurno`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.setup_turno: ~12 rows (aproximadamente)
DELETE FROM `setup_turno`;
/*!40000 ALTER TABLE `setup_turno` DISABLE KEYS */;
INSERT INTO `setup_turno` (`idSetupTurno`, `created_at`, `updated_at`, `nombre`, `lluvia`) VALUES
	(1, NULL, NULL, 'Turno 1', 100),
	(2, NULL, NULL, 'Turno 2', 200),
	(3, NULL, NULL, 'Turno 3', 300),
	(4, NULL, NULL, 'Turno 4', 200),
	(5, NULL, NULL, 'Turno 5', 0),
	(6, NULL, NULL, 'Turno 6', 0),
	(7, NULL, NULL, 'Turno 7', 0),
	(8, NULL, NULL, 'Turno 8', 0),
	(9, NULL, NULL, 'Turno 9', 100),
	(10, NULL, NULL, 'Turno 10', 100),
	(11, NULL, NULL, 'Turno 11', 200),
	(12, NULL, NULL, 'Turno 12', 200);
/*!40000 ALTER TABLE `setup_turno` ENABLE KEYS */;

-- Volcando estructura para tabla db_simulador_cuenca.turno
DROP TABLE IF EXISTS `turno`;
CREATE TABLE IF NOT EXISTS `turno` (
  `idTurno` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idJuego` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lluvia` int(11) DEFAULT NULL,
  `caudalFinal` double DEFAULT NULL,
  `estado` enum('iniciado','pausado','terminado','no_usado') COLLATE utf8_unicode_ci NOT NULL,
  `caudalIntermedio` double DEFAULT NULL,
  PRIMARY KEY (`idTurno`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.turno: ~12 rows (aproximadamente)
DELETE FROM `turno`;
/*!40000 ALTER TABLE `turno` DISABLE KEYS */;
INSERT INTO `turno` (`idTurno`, `nombre`, `idJuego`, `created_at`, `updated_at`, `lluvia`, `caudalFinal`, `estado`, `caudalIntermedio`) VALUES
	(1, 'Turno 1', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 100, 0, 'iniciado', NULL),
	(2, 'Turno 2', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 200, 0, 'no_usado', NULL),
	(3, 'Turno 3', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 300, 0, 'no_usado', NULL),
	(4, 'Turno 4', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 200, 0, 'no_usado', NULL),
	(5, 'Turno 5', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 0, 0, 'no_usado', NULL),
	(6, 'Turno 6', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 0, 0, 'no_usado', NULL),
	(7, 'Turno 7', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 0, 0, 'no_usado', NULL),
	(8, 'Turno 8', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 0, 0, 'no_usado', NULL),
	(9, 'Turno 9', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 100, 0, 'no_usado', NULL),
	(10, 'Turno 10', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 100, 0, 'no_usado', NULL),
	(11, 'Turno 11', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 200, 0, 'no_usado', NULL),
	(12, 'Turno 12', 1, '2017-12-01 20:47:45', '2017-12-01 20:47:45', 200, 0, 'no_usado', NULL);
/*!40000 ALTER TABLE `turno` ENABLE KEYS */;

-- Volcando estructura para tabla db_simulador_cuenca.turno_decision
DROP TABLE IF EXISTS `turno_decision`;
CREATE TABLE IF NOT EXISTS `turno_decision` (
  `idTurnoDecision` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idDecision` int(11) DEFAULT NULL,
  `idTurno` int(11) DEFAULT NULL,
  `idJugador` int(11) DEFAULT NULL,
  `activo` enum('si','no') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idTurnoDecision`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_simulador_cuenca.turno_decision: ~0 rows (aproximadamente)
DELETE FROM `turno_decision`;
/*!40000 ALTER TABLE `turno_decision` DISABLE KEYS */;
/*!40000 ALTER TABLE `turno_decision` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
